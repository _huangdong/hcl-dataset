import csv
import re
import os
import shutil
import pandas as pd
from IPython.display import display
import random
from typing import NamedTuple

from tap import Tap

from hcl_date_estim.dataset.versioned_directory import VersionedDirectory

# sheet 1
SHEET1 = {
    'csv': '/home/dh146/projects/hcl-raw-data/annotations/annotations-1.csv',
    'col_date': 4,
    'col_decade': 5,
    'col_hclid': 0,
}

# sheet 2 is not applicable.

# sheet 3
SHEET3 = {
    'csv': '/home/dh146/projects/hcl-raw-data/annotations/annotations-3.csv',
    'col_date': 8,
    'col_decade': 9,
    'col_hclid': 0,
}


class ArgumentParser(Tap):
    import_from: str = 'home'  # 'home' or 'repo'
    copy_method: str = 'copy'  # 'copy' or 'link'
    report_only: bool = False  # Only report but not output to disk
    dataset_config: int = 0  # 0 - val20,test0; 1 - val10,test10
    n_classes: int = 10  # Number of classes. Only effective when dataset_config=2.
    n_val: int = 20  # Number of instances of each class in validation set.
    n_test: int = 0  # Number of instances of each class in test set.
    output_dir: str = '/Scratch/dh146/hcl-data'  # Output directory
    excel: bool = False  # Write import result to Excel files
    force: bool = False  # Force writing to output
    old_style: bool = False  # Build old-style directory based dataset.
    no_pack: bool = False  # Do not pack the output into VersionedDirectory

    def __init__(self, *args, underscores_to_dashes: bool = False, explicit_bool: bool = False, **kwargs):
        super().__init__(*args, underscores_to_dashes=underscores_to_dashes, explicit_bool=explicit_bool, **kwargs)

    def add_arguments(self) -> None:
        self.add_argument('-o', '--output_dir')
        self.add_argument('-n', '--n_classes')


class PathConfig(NamedTuple):
    input_dir: str
    filename_format1: str
    filename_format2: str
    output_dir: str
    pack: bool = True


class DatasetConfig(NamedTuple):
    n_val: int
    n_test: int
    n_classes: int
    even_split: bool = False
    first_year: int = 1900
    last_year: int = 1979


def build_csv_dataset(successfuls, columns, args, path_config, dataset_config):
    df = pd.DataFrame(successfuls, columns=columns)

    output_dir = os.path.join(path_config.output_dir, 'hcl-dataset')

    if os.path.exists(output_dir) and not args.force:
        print("Target directory already exists. Will not proceed.")
    else:
        os.makedirs(output_dir)

    df['Instance'] = df['HCL ID'].apply(lambda x: x + '.png')
    # df.drop(['Src', 'HCL ID'], inplace=False, axis=1).to_csv(os.path.join(output_dir, 'hcl.csv'))

    df = df.sample(frac=1).reset_index(drop=True)
    df[['Instance', 'Precision', 'Year']].to_csv(os.path.join(output_dir, 'hcl.csv'))

    # copy files
    images_dir = os.path.join(output_dir, 'images')
    if not os.path.exists(images_dir):
        os.makedirs(images_dir)

    dst_files = df['HCL ID'].apply(lambda x: os.path.join(images_dir, x + '.png'))
    for src, dst in zip(df['Src'], dst_files):
        if args.copy_method == 'link':
            os.symlink(src, dst)
        else:
            shutil.copy2(src, dst)

    if path_config.pack:
        VersionedDirectory.pack(output_dir, path_config.output_dir, 'hcl')


def main():
    args = ArgumentParser().parse_args()

    if args.import_from == 'home':
        path_config = PathConfig(
            input_dir='/Scratch/dh146/hcl-raw/resized-512x512',
            filename_format1='{}.tif.png',
            filename_format2='{}.tiff.png',
            output_dir=args.output_dir,
            pack=not args.no_pack,
        )
    else:
        path_config = PathConfig(
            input_dir='/research/repository/hcl/tif_files',
            filename_format1='{}.tif',
            filename_format2='{}.tiff',
            output_dir=args.output_dir,
            pack=not args.no_pack,
        )

    if args.dataset_config == 0:
        dataset_config = DatasetConfig(n_val=20, n_test=0, n_classes=10)
    elif args.dataset_config == 1:
        dataset_config = DatasetConfig(n_val=10, n_test=10, n_classes=10)
    elif args.dataset_config == 2:
        dataset_config = DatasetConfig(n_val=20, n_test=0,
                                       n_classes=args.n_classes)
    elif args.dataset_config == 3:
        dataset_config = DatasetConfig(n_val=args.n_val, n_test=args.n_test,
                                       n_classes=args.n_classes)
    else:
        dataset_config = None

    successfuls1, unsuccessfuls1 = import_data(SHEET1, path_config)
    successfuls3, unsuccessfuls3 = import_data(SHEET3, path_config)

    # Merge the results
    df1 = pd.DataFrame(successfuls1, columns=["HCL ID", "Year", "Precision", "File Size", "Src"])
    df3 = pd.DataFrame(successfuls3, columns=["HCL ID", "Year", "Precision", "File Size", "Src"])
    grouped = pd.concat([df1, df3]).groupby('HCL ID')
    sizes = grouped.size()
    first = grouped.first()
    last = grouped.last()
    conflict_status = [(hclid, f, l) for (s, hclid, f, l) in zip(sizes, first.T, first['Year'], last['Year']) if
                       s > 1 and f != l and f != '' and l != '']

    if len(conflict_status) > 0:
        print("Warning: Conflict in merged results.")
        df_conflict = pd.DataFrame(conflict_status, columns=['HCL ID', 'Label1', 'Label3'])
        df_conflict.set_index('HCL ID')
        #     df_conflict.title = 'Conflict in merged results. Label1 will be used.'
        display((df_conflict,))
        write_conflict_latex([pd.read_csv(sheet['csv'], encoding='latin1',
                                          keep_default_na=False)
                              for sheet in [SHEET1, SHEET3]],
                             df_conflict,
                             'conflict_details.tex',
                             args.output_dir)

    successfuls = first.reset_index().values.tolist()
    unsuccessfuls = unsuccessfuls1 + unsuccessfuls3

    columns_succ = ["HCL ID", "Year", "Precision", "File Size", "Src"]
    columns_unsuc = ["HCL ID", "Reason", "Extra Info"]

    save_to_csv(successfuls, columns_succ, 'successful-entries.csv',
                output_dir=args.output_dir)
    if args.excel:
        save_to_excel(successfuls, columns_succ, '../generated/successful-entries.xlsx',
                      output_dir=args.output_dir)

    save_to_csv(unsuccessfuls, columns_unsuc, 'unsuccessful-entries.csv',
                output_dir=args.output_dir)
    if args.excel:
        save_to_excel(unsuccessfuls, columns_unsuc, '../generated/unsuccessful-entries.xlsx',
                      output_dir=args.output_dir)

    export_table = [{'year': i[1], 'src': i[4]} for i in successfuls]

    if args.old_style:
        build_dataset_decade_withvaltest(export_table, args, path_config, dataset_config)
    else:
        build_csv_dataset(successfuls, columns_succ, args, path_config, dataset_config)

    return successfuls, unsuccessfuls


def drop_empty_columns(df) -> pd.DataFrame:
    # Find the columns where each value is null
    empty_cols = [col for col in df.columns if df[col].isnull().all() or (df[col] == '').all()]
    # Drop these columns from the dataframe
    return df.drop(empty_cols, axis=1, inplace=False)


def write_conflict_latex(dfs: list, df_conflict: pd.DataFrame, file_name: str, output_dir: str = '.'):
    df_tab = pd.DataFrame(columns=['HCL ID', 'Annotation Source', 'Date', 'Decade'])
    dfs_trim = [drop_empty_columns(df) for df in dfs]
    for _, row in df_conflict.iterrows():
        hclid = row['HCL ID']
        for sheet_id, df_orig in enumerate(dfs_trim):
            col_id = df_orig.columns[0]
            df_filtered = df_orig[df_orig[col_id] == hclid]
            for i, row_filtered in df_filtered.iterrows():
                df_tab.loc[len(df_tab.index) + 1] = [
                    row_filtered[col_id],
                    f'Sheet {sheet_id}: {i + 1}',
                    row_filtered['Date'],
                    row_filtered['Decade']
                ]
    with open(os.path.join(output_dir, file_name), 'w') as f:
        f.write(df_tab.to_latex(
            index=False, longtable=True,
            caption='Details about the conflict during merging the annotation sheets.',
            label='tab:merge-confliction-details'
        ))


def save_to_excel(table, columns, file_name, output_dir='.', sheet_name='sheet1'):
    df = pd.DataFrame(table, columns=columns)
    df.to_excel(os.path.join(output_dir, file_name), sheet_name=sheet_name, index=False)


def save_to_csv(table, columns, file_name, output_dir='.'):
    df = pd.DataFrame(table, columns=columns)
    df.to_csv(os.path.join(output_dir, file_name))


def convert_to_4digit(year: str, decade: str) -> (bool, str, str):
    if re.match(r'^\d{,4}$', year) is None:
        return False, '', 'NonDigitYear'
    if len(year) == 2:
        # Assuming decade string must be provided when the year is 2-digit.
        if decade is not None and decade != '':
            return True, decade[:2] + year, ''
        elif int(year) > 20:
            # FIXME: A naive assumption that all years like "69" is a shortened "1969" for years later than 1920.
            return True, '19' + year, ''
        else:
            # Ambiguous case: 2-digit year within 20 and no decade information provided.
            return False, '', 'AmbiguousShortYear'
    elif len(year) == 4:
        if decade is None or re.match(r'^\s*$', decade) is not None:
            return True, year, ''
        else:
            if year[:3] == decade[:3]:
                return True, year, ''
            else:
                return False, year, 'ConflictYearDecade'
    else:
        # No photos taken in the Middle Ages in this island.
        return False, '', '3DigitYear'


class AnnotationDate:
    def __init__(self, valid=False, year=None, precision=None, decade=None) -> None:
        super().__init__()

        # print("before: valid={}, year={}, decade={}".format(valid, year, decade))
        self.err_msg = ''
        if year is not None and year != '' or decade is not None and decade != '':
            self.valid, self.year, self.err_msg = convert_to_4digit(year, decade)
        else:
            self.valid = False
            self.year = None
            self.err_msg = 'EmptyYearAndDecade'
        # print("after: valid={}, year={}".format(self.valid, self.year))

        self.precision = precision

    def decade(self):
        if self.valid:
            return self.year[:3] + '0'
        else:
            return ''


def get_year_from_raw_label(date: str, decade: str) -> AnnotationDate:
    # Annotation file from HCL contains a variety of loosely defined formats in labeling dates.
    # Have to test for each possible format one-by-one.
    re_handlers = [
        (re.compile(r'^\s*(\d{4})\s*$'),
         lambda match, decade: AnnotationDate(True, match.group(1), '1y', decade)),
        (re.compile(r'\w*\s*\.*\s*(\d{4})[sS]?'),
         lambda match, decade: AnnotationDate(True, match.group(1), '1y', decade)),
        (re.compile(r'\w*\s*\d{0,2}[- ]?\w{3}[- ](\d{4}|\d{2})'),
         lambda match, decade: AnnotationDate(True, match.group(1), '1y', decade)),
        (re.compile(r'^\s*$'),
         lambda match, decade: AnnotationDate(valid=True,
                                              year=re.sub(r'^\s*(\d{3}).*$', r'\g<1>5', decade),
                                              precision='10y',
                                              decade=decade))
    ]

    for regexp, handler in re_handlers:
        m = regexp.match(date)
        if m is not None:
            # print("  matching", regexp.pattern)
            return handler(m, decade)

    # print("  matches NOTHING.")
    return AnnotationDate(False)


def import_data(sheet: dict, path_config: PathConfig) -> (list, list):
    successfuls = []
    unsuccessfuls = []

    annotations = []
    with open(sheet['csv'], encoding='utf-8', errors='ignore') as csvfile:
        reader = csv.reader(csvfile, dialect=csv.excel)
        for row in reader:
            annotations.append(row)

    # We do not need the title line.
    annotations = annotations[1:]

    print("Total {} records.".format(len(annotations)))

    for entry in annotations:
        # print('\n\n')
        hclid = entry[sheet['col_hclid']]
        hclid = re.sub(r'\s', '', hclid)
        # print("HCL ID=", hclid)

        label = get_year_from_raw_label(entry[sheet['col_date']], entry[sheet['col_decade']])
        if not label.valid:
            unsuccessfuls.append((hclid, label.err_msg, entry[sheet['col_date']] + '///' + entry[sheet['col_decade']]))
            continue

        # At this point we know the labeled year, as well as the precision.
        # But we do not make use of the precision yet.
        src = ''
        for fmt in [path_config.filename_format1, path_config.filename_format2]:
            hclids = [hclid, hclid[:4] + hclid[4:].upper(), hclid[:4] + hclid[4:].lower()]
            for hclid_ in hclids:
                try:
                    src = os.path.join(path_config.input_dir, fmt.format(hclid_))
                except ValueError as e:
                    print(fmt)
                    raise e
                if os.path.exists(src):
                    break
            if os.path.exists(src):
                break

        try:
            file_size = os.stat(src).st_size
            successfuls.append((hclid, label.year, label.precision, file_size, src))
            # print("Success processing {}".format(hclid))
        except OSError as err:
            unsuccessfuls.append((hclid, type(err).__name__, label.year))
            # print("{} (errno {}) occurred when processing {}".format(type(err).__name__, err.errno, hclid))

    print("\n\n", len(unsuccessfuls), "unsuccessful entries out of", len(annotations), "entries.")
    # for i in unsuccessfuls:
    #     print(i)

    return successfuls, unsuccessfuls


def build_dataset_decade_withvaltest(table, args: ArgumentParser, path_config: PathConfig,
                                     dataset_config: DatasetConfig):
    output_dir = os.path.join(path_config.output_dir, 'by-decade')

    n_val = dataset_config.n_val
    n_test = dataset_config.n_test

    if os.path.exists(output_dir) and not args.force:
        print("Target directory already exists. Will not proceed.")
    else:
        os.makedirs(output_dir)

    dataset = {}

    if not dataset_config.even_split:
        first_decade = dataset_config.first_year
        last_decade = dataset_config.last_year
        pre_key = str(first_decade - 1) + '-'
        post_key = str(last_decade + 1) + '+'
        dataset[pre_key] = [i['src'] for i in table if int(i['year']) < first_decade]
        dataset[post_key] = [i['src'] for i in table if int(i['year']) > last_decade]
        for decade in range(first_decade, last_decade, 10):
            decade = str(decade)
            dataset[decade] = [i['src'] for i in table if i['year'][:3] == decade[:3]]
    else:
        n_inst_per_class = int(len(table) / dataset_config.n_classes) + 1
        for i in range(dataset_config.n_classes):
            max_index = min(len(table), n_inst_per_class)
            print(f'{len(table)=}', f'{n_inst_per_class=}', f'{max_index=}')
            subtable = table[:max_index]
            table = table[max_index:]
            dataset['c' + str(i)] = [x['src'] for x in subtable]

    classes = dataset.keys()

    # shuffle each bin
    for k in classes:
        random.shuffle(dataset[k])

    # separate the dataset
    dataset_train = {}
    dataset_val = {}
    dataset_test = {}

    for k in classes:
        dataset_test[k] = dataset[k][n_val:(n_val + n_test)]
        dataset_train[k] = dataset[k][(n_val + n_test):]
        dataset_val[k] = dataset[k][:n_val]

    # copy files
    for k in classes:
        for sub_name, sub_set in zip(['train', 'val', 'test'],
                                     [dataset_train, dataset_val, dataset_test]):
            if len(sub_set) == 0:
                continue

            dst = os.path.join(output_dir, sub_name, k)
            for f in sub_set[k]:
                try:
                    if not os.path.exists(dst):
                        os.makedirs(dst)
                    dst_file = os.path.join(dst, os.path.basename(f))
                    if args.copy_method == 'link':
                        os.symlink(f, dst_file)
                    else:
                        shutil.copy2(f, dst)
                except OSError as err:
                    print('Error in handling', f)


if __name__ == '__main__':
    result = main()
