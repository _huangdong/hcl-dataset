from PIL import Image
import os
import os.path
import numpy as np
import sys
import csv
import logging

from torchvision.datasets import DatasetFolder
from torchvision.datasets.vision import VisionDataset
from torchvision.datasets.folder import default_loader, pil_loader, accimage_loader
from torchvision.datasets.utils import check_integrity, download_and_extract_archive
import torchvision.transforms

_cached_all_images = None
_cached_valid_train_ids = None
_cached_valid_val_ids = None
_cached_valid_test_ids = None


def _read_all_columns(path, index, has_header=True):
    values = list()
    with open(path, encoding='utf-8', errors='ignore') as f:
        logging.info(f'Reading file {path}')
        reader = csv.reader(f, dialect=csv.excel)
        if has_header:
            next(reader)
        for row in reader:
            values.append(row[index])
    return values


class DewImportError(Exception):
    pass


def _compose_samples(meta_info, ids):
    samples = list()
    for img_id in ids:
        img_info = meta_info[img_id]
        path = img_info['path']
        year = img_info['year']
        class_idx = (int(year) - Dew.year_start) // Dew.year_interval
        if class_idx >= Dew.n_classes:
            raise DewImportError('Invalid class index {} caused by img_id {}: GT={}'.format(
                class_idx, img_id, year
            ))
        item = (path, class_idx)
        samples.append(item)

    return samples


def years_of(dataset: str):
    if dataset == 'train':
        ids = _cached_valid_train_ids
    elif dataset == 'val':
        ids = _cached_valid_val_ids
    elif dataset == 'test':
        ids = _cached_valid_test_ids
    else:
        raise ValueError(f'Invalid dataset {dataset}.')

    assert ids is not None, 'Dataset DEW has not been initialized. Call dew_dataset()' \
                            'to build the dataset first.'

    return [int(_cached_all_images[i]['year']) for i in ids]


def _make_dataset(root):
    # Get information of all images
    all_images = dict()
    with open(os.path.join(root, Dew.meta_root, Dew.meta_csv),
              encoding='utf-8', errors='ignore') as f:
        reader = csv.reader(f, dialect=csv.excel)
        header = next(reader)

        c_id = header.index('img_id')
        c_gt = header.index('GT')
        c_date = header.index('date_taken')
        c_gran = header.index('date_granularity')

        for row in reader:
            img_id = row[c_id]
            year = row[c_gt]
            path = os.path.join(root, Dew.images_root,
                                img_id[0], img_id[1:3], img_id + '.jpg')
            all_images[img_id] = {'year': year, 'path': path}

    # Get the missing images
    missing_ids = _read_all_columns(os.path.join(
        root, Dew.images_root, Dew.missing_images_csv), 0, has_header=False)
    missing_ids_set = set(missing_ids)

    # Get the validation images
    val_ids = _read_all_columns(os.path.join(
        root, Dew.meta_root, Dew.val_csv), 1)

    # Get the test images
    test_ids = _read_all_columns(os.path.join(
        root, Dew.meta_root, Dew.test_csv), 1)

    valid_val_ids = [x for x in val_ids if x not in missing_ids_set]
    logging.info(f"valid_val_ids generated. {len(valid_val_ids)=}")

    valid_test_ids = [x for x in test_ids if x not in missing_ids_set]
    logging.info(f"valid_test_ids generated. {len(valid_test_ids)=}")

    non_train_ids_set = set(valid_val_ids).union(set(valid_test_ids)).union(missing_ids_set)
    valid_train_ids = [x for x in all_images.keys() if x not in non_train_ids_set]
    logging.info(f"valid_train_ids generated. {len(valid_train_ids)=}")
    logging.info("All ids prepared.")

    train_images = _compose_samples(all_images, valid_train_ids)
    logging.info("train images composed.")
    val_images = _compose_samples(all_images, valid_val_ids)
    logging.info("val images composed.")
    test_images = _compose_samples(all_images, valid_test_ids)
    logging.info("test images composed.")

    global _cached_all_images, _cached_valid_val_ids, _cached_valid_test_ids, \
        _cached_valid_train_ids

    _cached_all_images = all_images
    _cached_valid_train_ids = valid_train_ids
    _cached_valid_val_ids = valid_val_ids
    _cached_valid_test_ids = valid_test_ids

    print("DEW: The entire DEW dataset has total {} images defined, {} missing in this copy.\n"
          "\tValidation set: {} out of {} are missing.\n"
          "\tTest set: {} out of {} are missing.\n"
          "\t\nNumber of valid images in this copy:\n"
          "\t\tTrain set: {}\n"
          "\t\tValidation set: {}\n"
          "\t\tTest set: {}".format(
            len(all_images.keys()), len(missing_ids),
            len(val_ids) - len(valid_val_ids), len(val_ids),
            len(test_ids) - len(valid_test_ids), len(test_ids),
            len(valid_train_ids),
            len(valid_val_ids),
            len(valid_test_ids)))

    return train_images, val_images, test_images


def dew_dataset(root, train=True, val=True, test=False, **kwargs):
    train_samples, val_samples, test_samples = _make_dataset(root)

    train_kwargs = {'transform': kwargs.pop('train_transform', None)}
    val_kwargs = {'transform': kwargs.pop('val_transform', None)}
    test_kwargs = {'transform': kwargs.pop('test_transform', None)}

    datasets = list()
    if train:
        datasets.append(Dew(root, train_samples, **dict(kwargs, **train_kwargs)))
    if val:
        datasets.append(Dew(root, val_samples, **dict(kwargs, **val_kwargs)))
    if test:
        datasets.append(Dew(root, test_samples, **dict(kwargs, **test_kwargs)))

    return datasets


class Dew(VisionDataset):
    # For online-download (not implemented yet)
    src_url = 'http://weka9.resnet.cms.waikato.ac.nz/datasets/dew/'
    version = 1

    # For local storage
    images_root = 'images'
    meta_root = '10.22000-43/data/dataset'

    missing_images_csv = 'missing_images.csv'
    meta_csv = 'meta.csv'
    test_csv = 'test_images_1120_shuffeled.csv'
    val_csv = 'validation_images_8495.csv'

    year_start = 1930
    year_interval = 5
    n_classes = 14

    def __init__(self, root: str, samples: list, transform=None,
                 loader=None,
                 url=src_url, cache=False, cache_path='/tmp/DEW-cached',
                 cache_limit=1024 * 1024 * 1024):
        super(Dew, self).__init__(root=root, transforms=transform)
        print(f'{root=}\n{len(samples)=}\n{transform=}\n{loader=}')

        self.classes = [str(x) for x in range(self.year_start,
                                              self.year_start + self.n_classes * self.year_interval,
                                              self.year_interval)]
        self.class_to_idx = {n: i for i, n in enumerate(self.classes)}
        print("DEW class_to_idx:", self.class_to_idx)
        self.transform = transform
        # self.target_transform = torchvision.transforms.ToTensor()
        self.samples = samples
        self.targets = [s[1] for s in samples]

        if loader is None:
            self.loader = default_loader

    def __len__(self):
        return len(self.samples)

    def __getitem__(self, index):
        """
        Args:
            index (int): Index

        Returns:
            tuple: (sample, target) where target is class_index of the target class.
        """
        path, target = self.samples[index]
        sample = self.loader(path)
        if self.transform is not None:
            sample = self.transform(sample)
        if self.target_transform is not None:
            target = self.target_transform(target)

        return sample, target

    def download(self):
        pass
