import os
import hashlib
import lzma
import tarfile
import json
from urllib.parse import urljoin
from typing import NamedTuple, Union, List
import logging
from torchvision.datasets.vision import VisionDataset
from torchvision.datasets.utils import download_and_extract_archive


class _FileInfo(NamedTuple):
    name: str
    md5: str = None
    size: int = None


class _PathInfo(NamedTuple):
    path: str
    files: List[Union[dict, _FileInfo]]


class PkgInfo(NamedTuple):
    version: int
    creation_date: str
    contents: List[Union[dict, _PathInfo]]


class VersionedDirectory:
    root: str
    src_url: str
    archive_name: str
    version: int
    desc_file = 'info.json.xz'
    directory_ready = False

    def __init__(self, root: str = None, **kwargs):
        if root is not None:
            type(self).root = root

    @classmethod
    def set_version(cls, version: int):
        cls.version = version

    @classmethod
    def set_up(cls, **kwargs):
        if cls.directory_ready:
            raise RuntimeError("The directory can only be set up once."
                               " It has been set up.")

        if 'root' in kwargs:
            cls.root = kwargs['root']
        if 'version' in kwargs:
            cls.version = kwargs['version']

        if cls._check_integrity():
            print(f'Local directory {cls.root} is ready with version {cls.version}.')
            return

        print(f'Local directory {cls.root} does not exist or not match'
              f' the version {cls.version}. Starting downloading...')
        cls.download()
        if not cls._check_integrity():
            raise RuntimeError('Dataset not found or corrupted.')

        cls.directory_ready = True

    @classmethod
    def get_info(cls, root: str):
        try:
            with lzma.open(os.path.join(root, cls.desc_file)) as f:
                info = PkgInfo(**json.load(f))
                contents = []
                for path in info.contents:
                    path = _PathInfo(**path)
                    files = []
                    for file in path.files:
                        file = _FileInfo(**file)
                        files.append(file)
                    path = _PathInfo(**dict(path._asdict(), files=files))  # FIXME: ugly
                    contents.append(path)
                info = PkgInfo(**dict(info._asdict(), contents=contents))  # FIXME: ugly
        except FileNotFoundError:
            info = None

        if info is not None:
            logging.info(f'Directory info: {cls.archive_name} version {info.version}')
        return info

    @classmethod
    def _check_integrity(cls):
        info = cls.get_info(cls.root)
        if info is None or info.version != cls.version:
            return False

        for path in info.contents:
            for file in path.files:
                file_name = os.path.join(cls.root, path.path, file.name)
                result = True
                reason = ''
                if not os.path.isfile(file_name):
                    result = False
                    reason = "File doesn't exist."

                if file.md5 is None and file.size is None:
                    result = False
                    reason = 'Either "md5" or "size" must be specified.'
                elif file.md5 is not None and file.md5 != cls._compute_md5(file_name):
                    result = False
                    reason = f"Calculated MD5 {cls._compute_md5(file_name)} " \
                             f"doesn't match {file.md5} in package definition."
                elif file.size is not None and file.size != os.path.getsize(file_name):
                    result = False
                    reason = f"File size {os.path.getsize(file_name)} doesn't" \
                             f"match {file.size} in package definition."

                if result is False:
                    print(f'Integrity check failed at file {file_name}.\n'
                          f'Reason: {reason}')
                    return False

        return True

    @classmethod
    def download(cls):
        file_name = r'{}.v{}.tar.xz'.format(cls.archive_name, cls.version)
        url = urljoin(cls.src_url, file_name)
        download_and_extract_archive(url, cls.root, filename=file_name,
                                     remove_finished=True)

    @classmethod
    def pack(cls, root_dir: str, output_dir: str, archive_name: str = None,
             version: int = None, block_size: int = 65536,
             dont_hash_dirs: tuple = ('images',)):
        from datetime import datetime

        now = datetime.now()
        creation_date = now.strftime("%Y-%m-%d %H:%M:%S")

        if archive_name is not None:
            cls.archive_name = archive_name

        if version is None:
            info = cls.get_info(root_dir)
            if info is not None and info.version < 2000_0000:
                version = info.version + 1
            else:
                version = int(now.strftime('%Y%m%d'))

        cls.write_meta(root_dir, version, creation_date, dont_hash_dirs=dont_hash_dirs)

        output_file = os.path.join(output_dir, r'{}.v{}.tar.xz'.format(
            archive_name, version))

        print('')
        with tarfile.open(output_file, 'w:xz', preset=9) as tar:
            file_list = os.listdir(root_dir)
            print('Creating the tar file package. This may take a long while.')
            for i, file in enumerate(file_list):
                tar.add(os.path.join(root_dir, file), arcname=file)

        print(f'Directory {root_dir} has been successfully packed.\n'
              f'Version: {version}')

    @classmethod
    def write_meta(cls, root_dir: str, version: int, creation_date: str,
                   block_size: int = 65536, dont_hash_dirs: tuple = ('images',)):
        contents = []
        for path, _, files in os.walk(root_dir):
            subcontents = []

            is_dir_without_hash = False
            for excluded_dir in dont_hash_dirs:
                if os.path.join(root_dir, excluded_dir) in path:
                    is_dir_without_hash = True
                    break
            for name in files:
                if name in (cls.desc_file,):
                    continue
                full_name = os.path.join(path, name)
                if is_dir_without_hash:
                    subcontents.append({'name': name,
                                        'size': os.path.getsize(full_name)})
                else:
                    md5 = hashlib.md5()
                    with open(full_name, 'rb') as f:
                        for block in iter(lambda: f.read(block_size), b''):
                            md5.update(block)
                    subcontents.append({'name': name, 'md5': md5.hexdigest()})

            contents.append({'path': path[len(root_dir):].lstrip('/'),
                             'files': subcontents})

        data = {'version': version, 'creation_date': creation_date,
                'contents': contents}
        with lzma.open(os.path.join(root_dir, cls.desc_file), 'wb') as f:
            # json.dump() doesn't directly work with the lzma backend.
            f.write(bytes(json.dumps(data, indent=4), encoding='utf-8'))

        logging.debug(f"Meta file {os.path.join(root_dir, cls.desc_file)}"
                      f"successfully written.")

    @staticmethod
    def _compute_md5(file_name: str, block_size=65536):
        md5 = hashlib.md5()
        with open(file_name, 'rb') as f:
            for block in iter(lambda: f.read(block_size), b''):
                md5.update(block)

        return md5.hexdigest()

    @classmethod
    def reflection(cls):
        print(f'{cls=}, {cls.__dict__=}')


def _test():
    class Hcl(VersionedDirectory):
        root = '/tmp/versioned-directory-test/'
        src_url = 'http://weka9.resnet.cms.waikato.ac.nz/datasets/hcl/'
        archive_name = 'hcl'

        def __init__(self, root: str = None, version: int = None):
            super().__init__(root)
            if version is not None:
                self.set_version(version)
            self.reflection()

    logging.basicConfig(level=logging.INFO,
                        format='%(asctime)s %(levelname)-8s %(message)s')
    # Hcl.pack('/Scratch/dh146/hcl-data-512', '/var/www/html/datasets',
    #          'hcl', 1)
    # Hcl.pack('/Scratch/dh146/hcl-data-512',
    #          '/var/www/html/datasets', 'hcl')

    hcl = Hcl(root='/tmp/yet-another-test-dir', version=6)
    hcl.set_up()


if __name__ == '__main__':
    # _test()
    pass
