import os
import logging

import pandas as pd

from .versioned_directory import VersionedDirectory
from .versioned_dataset import VersionedDataset


class Hcl(VersionedDataset):
    version = 1
    versioned_directory = type('', (VersionedDirectory,), {
        'src_url': 'http://weka9.resnet.cms.waikato.ac.nz/datasets/hcl/',
        'archive_name': 'hcl'
    })

    df_train: pd.DataFrame
    df_val: pd.DataFrame
    df_test: pd.DataFrame

    regression: bool = False

    first_year: int
    last_year: int
    year_interval: int

    def __init__(self, root, samples, transforms=None, **kwargs):
        super().__init__(root=root, samples=samples, transforms=transforms, **kwargs)

    @classmethod
    def _split_by_precision(cls, df: pd.DataFrame, shuffle=None):
        logging.info(f'{shuffle=}')
        # Split the df into two copies with different precision.
        df_1y = df[df['Precision'] == '1y'].copy()
        df_10y = df[df['Precision'] == '10y'].copy()

        if shuffle:
            # shuffle the two tables
            df_1y = df_1y.sample(frac=1).reset_index()
            df_10y = df_10y.sample(frac=1).reset_index()

        return df_1y, df_10y

    @classmethod
    def _make_classification_samples(cls, df: pd.DataFrame, n_val=20, n_test=0,
                                     n_classes=10, first_year=1900, last_year=1979,
                                     **kwargs):

        # Split the dataset into n classes
        time_period = last_year + 1 - first_year
        interval = int(float(time_period) / float(n_classes - 2))
        boundaries = [first_year + i * interval for i in range(n_classes - 1)]
        labels = [f'{i}' for i in boundaries[:-1]]
        cls.classes = [f'{boundaries[0] - 1}-'] + labels + [f'{boundaries[-1]}+']
        cls.class_to_idx = {n: i for i, n in enumerate(cls.classes)}

        cls.first_year = first_year
        cls.last_year = last_year
        cls.year_interval = interval

        def class_of(row):
            offset = min(max(int(row['Year'] - first_year), -1), time_period)
            return int(offset / interval + 1)

        df['Class'] = df.apply(class_of, axis=1)
        shuffle = kwargs.get('shuffle', None)
        df_1y, df_10y = cls._split_by_precision(df, shuffle=shuffle)

        columns = ['Instance', 'Year', 'Precision', 'Class']
        df_test = pd.DataFrame(columns=columns)
        df_val = df_test.copy()
        df_train = df_test.copy()
        for i in range(n_classes):
            instances = df_1y[df_1y['Class'] == i]
            df_test = df_test.append(instances[: n_test][columns])
            df_val = df_val.append(instances[n_test: n_val][columns])
            df_train = df_train.append(instances[n_val:][columns])
        df_train = df_train.append(df_10y[columns])

        return df_train, df_val, df_test

    @classmethod
    def _make_samples(cls, root, n_val=20, n_test=0, n_classes=10,
                      first_year=1900, last_year=1979,
                      regression=False, pure_regression_split=False,
                      **kwargs):
        cls.regression = regression

        df = pd.read_csv(os.path.join(root, 'hcl.csv'), index_col=0)
        df['Instance'] = df['Instance'].apply(lambda x: os.path.join(root, 'images', x))

        # Use the same split for both regression and classification in order
        # to fairly compare their performance.
        df_train, df_val, df_test = cls._make_classification_samples(
            df, n_val=n_val, n_test=n_test, n_classes=n_classes,
            first_year=first_year, last_year=last_year, **kwargs)

        if regression:
            df_1y, df_10y = cls._split_by_precision(df)
            if pure_regression_split:
                df_test = df_1y[:n_test]
                df_val = df_1y[n_test:(n_test + n_val)]
                df_train = df_1y[(n_test + n_val):]
                df_train = df_train.append(df_10y)

            columns = ['Instance', 'Year']
            df_train = df_train[columns]
            df_val = df_val[columns]
            df_test = df_test[columns]

            cls.first_year = df_val['Year'].min()
            cls.last_year = df_val['Year'].max()
            cls.year_interval = 10

            cls.first_year -= cls.first_year % cls.year_interval

            train_samples = df_train.values.tolist()
            val_samples = df_val.values.tolist()
            test_samples = df_test.values.tolist()
        else:
            train_samples = df_train[['Instance', 'Class']].values.tolist()
            val_samples = df_val[['Instance', 'Class']].values.tolist()
            test_samples = df_test[['Instance', 'Class']].values.tolist()

        cls.df_train = df_train
        cls.df_val = df_val
        cls.df_test = df_test

        return train_samples, val_samples, test_samples

    @classmethod
    def years_of(cls, dataset: str):
        if dataset == 'train':
            df = cls.df_train
        elif dataset == 'val':
            df = cls.df_val
        elif dataset == 'test':
            df = cls.df_test
        else:
            raise ValueError(f'Invalid dataset {dataset}.')

        assert df is not None, f'Dataset HCL has not been initialized. Call' \
                               f' {cls.__name__}.make_dataset() ' \
                               'to build the dataset first.'

        return df['Year'].values.tolist()
