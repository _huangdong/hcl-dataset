from abc import ABC, abstractmethod
from torchvision.datasets.vision import VisionDataset
from torchvision.datasets.folder import default_loader

from .versioned_directory import VersionedDirectory


class VersionedDataset(VisionDataset, ABC):
    versioned_directory = VersionedDirectory
    version: int

    def __init__(self, root, samples, transform=None,
                 loader=None, **kwargs):
        super().__init__(root, transforms=transform)
        self.transform = transform
        self.samples = samples

        if loader is None:
            self.loader = default_loader

    def __getitem__(self, index):
        path, target = self._get_sample(index)

        sample = self.loader(path)
        if self.transform is not None:
            sample = self.transform(sample)
        if self.target_transform is not None:
            target = self.target_transform(target)

        return sample, target

    def __len__(self):
        return len(self.samples)

    def _get_sample(self, index):
        return self.samples[index]

    @classmethod
    def make_dataset(cls, root, train=True, val=True, test=False,
                     **kwargs):
        vd = cls.versioned_directory
        if 'version' in kwargs:
            cls.version = kwargs['version']
        if not vd.directory_ready:
            vd.set_up(root=root, version=cls.version)

        train_kwargs = {'transform': kwargs.pop('train_transform', None)}
        val_kwargs = {'transform': kwargs.pop('val_transform', None)}
        test_kwargs = {'transform': kwargs.pop('test_transform', None)}

        train_samples, val_samples, test_samples = cls._make_samples(root, **kwargs)

        datasets = list()
        if train:
            datasets.append(cls(root, train_samples, **dict(kwargs, **train_kwargs)))
        if val:
            datasets.append(cls(root, val_samples, **dict(kwargs, **val_kwargs)))
        if test:
            datasets.append(cls(root, test_samples, **dict(kwargs, **test_kwargs)))

        return datasets

    @classmethod
    @abstractmethod
    def _make_samples(cls, root, **kwargs):
        pass
