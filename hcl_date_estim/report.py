import sys

import torch
import torch.utils.data
import torchvision.transforms as transforms
import torchvision
from sklearn.metrics import confusion_matrix

import main as m
from dataset import dew


def _print_help():
    print('Usage: {} checkpoint_file')


def report_additional(data_loader, model=None, ord_models=None,
                      ord_indices=None, criterion=None, args=None):
    # switch to evaluate mode
    if model is not None:
        model.eval()

    if ord_models is not None:
        for model in ord_models:
            model.eval()

    # We need a new loader which is not shuffled.
    dataset = data_loader.dataset
    serial_loader = torch.utils.data.DataLoader(
        dataset, batch_size=args.batch_size, shuffle=False, num_workers=4,
        pin_memory=True, sampler=None)

    with torch.no_grad():
        all_preds = torch.tensor([], device='cuda')  # FIXME
        for i, (images, target) in enumerate(serial_loader):
            images = images.cuda(args.gpu, non_blocking=True)
            target = target.cuda(args.gpu, non_blocking=True)

            # compute output
            if ord_models is None:
                output = model(images)
            else:
                sub_logits = []
                for model in ord_models:
                    sub_logits.append(model(images))
                sub_logits = torch.stack(sub_logits).cuda(args.gpu, non_blocking=True)
                output = m.compute_probabilities(sub_logits, len(ord_indices)).cuda(args.gpu, non_blocking=True)
            # loss = criterion(output, target)

            # _, preds = torch.max(output.data, 1)
            all_preds = torch.cat((all_preds, output), dim=0)

        stacked = torch.stack(
            (
                torch.tensor(dataset.targets, device='cuda'),
                all_preds.argmax(dim=1)
            ),
            dim=1
        ).to('cpu')

        s = len(dataset.classes)
        confusion_matrix = torch.zeros(s, s, dtype=torch.int64)
        for p in stacked:
            tl, pl = p.tolist()
            pl = (s - 1) if pl > s else pl
            confusion_matrix[tl, pl] += 1
        print(confusion_matrix)


def _dew_test_loader(root='/Scratch/dh146/DEW-dataset', batch_size=64, n_workers=1, crop_select=(0, 3, 4), advprop=False):
    # There is a bug in accimage that the FiveCrop() will not work with it.
    # Must set the backend to PIL.
    torchvision.set_image_backend('PIL')

    if advprop:
        normalize = transforms.Lambda(lambda img: img * 2.0 - 1.0)
    else:
        mean = [0.485, 0.456, 0.406]
        std = [0.229, 0.224, 0.225]
        normalize = transforms.Normalize(mean=mean, std=std)

    test_transform = transforms.Compose([
        transforms.Resize(256),
        transforms.FiveCrop(224),  # this is a list of PIL Images
        transforms.Lambda(lambda crops: torch.stack(
            [normalize(transforms.ToTensor()(crops[i]))
             for i in crop_select])),  # returns a 4D tensor
    ])
    dew_testset, = dew.dew_dataset(root, train=False, val=False, test=True,
                                   test_transform=test_transform)
    test_loader = torch.utils.data.DataLoader(
        dew_testset,
        batch_size=batch_size, shuffle=False,
        num_workers=n_workers, pin_memory=True)
    return test_loader


def main():
    if len(sys.argv) < 2:
        _print_help()
        sys.exit(1)

    args = [
        '--resume', sys.argv[1],
        '--epochs', '0',
    ]

    if '-p' in sys.argv:
        args.append('--pretrained')

    if '--ag' in sys.argv:
        args.extend(('-a', 'googlenet'))

    if '--ae0' in sys.argv:
        args.extend(('-a', 'efficientnet-b0'))

    if '-m' in sys.argv:
        metrics = sys.argv[sys.argv.index('-m') + 1]
    else:
        metrics = None

    if '-d' in sys.argv:
        dataset = sys.argv[sys.argv.index('-d') + 1]
        args.extend(('--dataset', dataset))
        args.extend(('--n_classes', '14'))
    else:
        dataset = 'hcl'

    if '--subset' in sys.argv:
        subset = sys.argv[sys.argv.index('--subset') + 1]
    else:
        subset = 'val'

    if '-b' in sys.argv:
        batch_size = sys.argv[sys.argv.index('-b') + 1]
    else:
        batch_size = 64

    if '--' in sys.argv:
        extra_args = sys.argv[sys.argv.index('--') + 1:]
    else:
        extra_args = []

    sys.argv = [sys.argv[0], '/Scratch/dh146/hcl-data-512/by-decade'] + args + extra_args
    model, train_loader, val_loader, criterion, args = m.main()

    cm = m.calc_confusion_matrix(val_loader, model, criterion=criterion, args=args)
    print(cm)
    acc = cm.trace().item() / cm.sum().item()
    print("Average accuracy: {acc:.2%}".format(acc=acc))
    qwk = m.calc_quadratic_weighted_kappa(cm)
    print("Quadratic weighted kappa:", qwk)

    report_additional(val_loader, model, criterion=criterion, args=args)

    val_outputs, val_targets = m.apply_model(val_loader, model)
    print(f'{val_outputs.shape=}')
    print(f'{val_outputs[:3, :14]=}')
    print(f'{val_outputs[1021:1024, :14]=}')
    print(f'{val_targets[:10]=}')
    val_outputs = val_outputs[:, :14]
    cm = confusion_matrix(val_targets.cpu().numpy(), val_outputs.argmax(dim=1).cpu().numpy())
    print(cm)
    m.report_grouped_metrics(outputs=val_outputs, targets=val_targets.to(val_outputs.device))

    outputs = targets = None
    data_loader = None
    y = None
    if metrics is not None:
        if subset == 'val':
            data_loader = val_loader
        elif subset == 'train':
            data_loader = train_loader
        elif subset == 'test':
            if dataset == 'dew':
                n_classes = 14
                five_crop_selector = [0, 3, 4]
                n_crops = len(five_crop_selector)
                dew_test_loader = _dew_test_loader(batch_size=batch_size,
                                                   crop_select=five_crop_selector)
                # outputs will be a 3d tensor: bs, n_crops, n_classes
                outputs, targets = m.apply_model(dew_test_loader, model)
                print(f'{outputs.shape=}')
                print(f'{outputs[:3, :14]=}')
                print(f'{outputs[1021:1024, :14]=}')
                print(f'{targets[:10]=}')
                assert len(outputs) == n_crops * len(targets),\
                    f"There should be {n_crops} times of output samples than targets."
                if outputs.shape[-1] != n_classes:
                    print('Trimming outputs with {} classes to {} classes.'.format(
                        outputs.shape[-1], n_classes
                    ))
                    outputs = outputs[:, :n_classes]
                outputs = outputs.view(-1, n_crops, n_classes).mean(1)
                targets = targets.to(outputs.device)
                y = torch.tensor(dew.years_of('test')).to(outputs.device)
                cm = confusion_matrix(targets.clone().cpu().numpy(), outputs.argmax(dim=1).clone().cpu().numpy())
                print(cm)

        m.report_grouped_metrics(metrics.split(','), data_loader=data_loader, model=model,
                                 y=y, outputs=outputs, targets=targets)


if __name__ == '__main__':
    main()
