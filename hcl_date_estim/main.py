import os
import random
import shutil
import time
from datetime import datetime
import warnings
import PIL
import logging
from collections import OrderedDict

import torch
import torch.nn as nn
import torch.nn.parallel
import torch.nn.functional as F
import torch.backends.cudnn as cudnn
import torch.optim
import torch.utils.data
import torch.utils.data.distributed
from torchvision import set_image_backend, get_image_backend
import torchvision.transforms as transforms
import torchvision.datasets as datasets
import torchvision.models as models

from torch.utils.tensorboard import SummaryWriter
import scipy.stats as stats
from tap import Tap

from efficientnet_pytorch import EfficientNet
from dataset import dew
from ordinal.cohen_kappa_cont import cohen_kappa_cont, QuadraticWeightedKappaLoss, \
    LinearWeightedKappaLoss
from dataset.hcl import Hcl
from ordinal.ordinal_clm import CumulativeLinkLayer


class ArgumentParser(Tap):
    data: str  # path to dataset
    arch: str = 'resnet18'  # model architecture (default: resnet18)
    workers: int = 4  # number of data loading workers (default: 4)
    epochs: int = 90  # number of total epochs to run
    start_epoch: int = 0  # manual epoch number (useful on restarts)
    batch_size: int = 256
    lr: float = 0.1  # initial learning rate
    momentum: float = 0.9  # momentum
    weight_decay: float = 1e-4  # weight decay (default: 1e-4)
    print_freq: int = 10  # print frequency (default: 10)
    resume: str = ''  # path to latest checkpoint (default: none)
    evaluate: bool = False  # evaluate model on validation set
    pretrained: bool = False  # use pre-trained model
    world_size: int = -1  # number of nodes for distributed training
    rank: int = -1  # node rank for distributed training
    dist_url: str = 'tcp://224.66.41.62:23456'  # url used to set up distributed training
    dist_backend: str = 'nccl'  # distributed backend
    seed: int = None  # seed for initializing training.
    gpu: int = None  # GPU id to use
    image_size: int = 224  # image size
    advprop: bool = False  # use advprop or not
    multiprocessing_distributed: bool = False
    ordinal: str = None
    dataset: str = None  # Designate a predefined dataset rather than providing in the DIR.
    n_classes: int = 10  # Specify the number of classes.
    stratified: bool = False  # Use a stratified sampler in training
    noinit: bool = False  # Skip the initialization of weights.
    no_alt_model: bool = False  # If specified, do not alter the last FC layer of the model and just load as is.
    lr_interval: int = 30  # Interval to adjust learning rate.
    show_batch_info: bool = False  # Show detailed information in each batch.
    last_fc: bool = False  # Only optimize the last layer. This option implies --pretrained.
    no_tensorboard: bool = False  # Disable tensorboard writing.
    report_mode: bool = False  # Set to report mode.
    transfer: str = None  # Transfer trained result for further training.
    loss_kappa: str = None
    model_digraph: str = ''  # Write the DiGraph of the model into the file.
    hcl_options: str = ''  # Extra options passed to Hcl.make_datasets()
    clm: bool = False  # Use the clm model.
    no_grouped_opt: bool = False  # Do not group learning rates.
    plot_grad: str = None  # Plot grad to the file specified.
    regression: bool = False  # Run in regression mode.

    def add_arguments(self) -> None:
        super().add_arguments()
        # Due to the limitation of the TAP module, we need the following to add
        # the short form of arguments, as well as long help messages.
        self.add_argument('data', metavar='DIR')
        self.add_argument('-a', '--arch')
        self.add_argument('-j', '--workers')
        self.add_argument('-b', '--batch-size', '--batch_size',
                          help='mini-batch size (default: 256), this is the total '
                               'batch size of all GPUs on the current node when '
                               'using Data Parallel or Distributed Data Parallel')
        self.add_argument('--lr', '--learning-rate')
        self.add_argument('--wd', '--weight-decay', dest='weight_decay')
        self.add_argument('-p', '--print-freq')
        self.add_argument('-e', '--evaluate')
        self.add_argument('--multiprocessing-distributed',
                          help='Use multi-processing distributed training to launch '
                               'N processes per node, which has N GPUs. This is the '
                               'fastest way to use PyTorch for either single node or '
                               'multi node data parallel training')
        self.add_argument('--ordinal',
                          help='Run ordinal classification with the order specified.'
                               'Example: --ordinal Cool,Mild,Hot where "Cool", "Mild" and'
                               '"Hot" are defined names of classes in the original dataset.')
        self.add_argument('--loss_kappa',
                          help='Use Cohen Kappa (continuous version) as the loss '
                               'function. Available values: "linear" or "quadratic"')

        self.add_argument('--no-alt-model')
        self.add_argument('--lr-interval')
        self.add_argument('--show-batch-info')
        self.add_argument('--last-fc')
        self.add_argument('--report-mode')


best_acc1 = 0
k_top = 2
PID = str(os.getpid())
writer = None


def check_dataset_capability(cap: str, args):
    if not args.dataset:
        return False

    if cap == 'regression':
        r = args.dataset.startswith('hcl_')
    else:
        r = False

    return r


def main():
    global writer
    print(f'{PID=}')
    # args = parser.parse_args()
    args = ArgumentParser(description='HCL Images Date Estimation Training').parse_args()
    print(args)

    set_image_backend('accimage')
    logging.basicConfig(level=logging.INFO, format='%(asctime)s %(module)s %(levelname)-8s %(message)s')

    if args.seed is not None:
        random.seed(args.seed)
        torch.manual_seed(args.seed)
        cudnn.deterministic = True
        warnings.warn('You have chosen to seed training. '
                      'This will turn on the CUDNN deterministic setting, '
                      'which can slow down your training considerably! '
                      'You may see unexpected behavior when restarting '
                      'from checkpoints.')

    if args.gpu is not None:
        warnings.warn('You have chosen a specific GPU. This will completely '
                      'disable data parallelism.')

    if args.dist_url == "env://" and args.world_size == -1:
        args.world_size = int(os.environ["WORLD_SIZE"])

    if args.last_fc:
        args.pretrained = True

    if args.report_mode:
        args.no_tensorboard = True

    if not args.no_tensorboard and args.dataset is None:
        writer = SummaryWriter()

    if args.regression:
        assert check_dataset_capability('regression', args), \
            'Regression mode is not compatible with current dataset.'
        args.n_classes = 1
        if args.clm:
            warnings.warn('Disabled option "clm" in regression mode.')
            args.clm = False

        if args.stratified:
            warnings.warn(
                'Stratified sample has not been implemented in regression '
                'mode yet.')
            args.stratified = False

    args.distributed = args.world_size > 1 or args.multiprocessing_distributed

    ngpus_per_node = torch.cuda.device_count()
    # TODO (DH): No multiprocessing implemented yet.
    # Simply call main_worker function
    result = main_worker(args.gpu, ngpus_per_node, args)
    return result


def load_efficientnet_weights_from(model, url=None, local_file=None, load_fc=True,
                                   args=None):
    """ Loads pretrained weights, and downloads if loading for the first time. """
    if local_file is not None:
        state_dict = torch.load(local_file)
    else:
        from torch.hub import load_state_dict_from_url
        state_dict = load_state_dict_from_url(url)

    state_dict = state_dict['state_dict']

    if load_fc:
        model.load_state_dict(state_dict)
    else:
        state_dict.pop('module._fc.weight')
        state_dict.pop('module._fc.bias')
        res = model.load_state_dict(state_dict, strict=False)
        assert set(res.missing_keys) == {'module._fc.weight', 'module._fc.bias'}, \
            'issue loading pretrained weights!\n' \
            f'{res.missing_keys=}'
    print('Loaded pretrained weights.')


def create_linear_init(in_features, out_features, init_weights=True):
    fc = nn.Linear(in_features, out_features)

    if init_weights:
        x = stats.truncnorm(-2, 2, scale=0.01)
        values = torch.as_tensor(x.rvs(fc.weight.numel()), dtype=fc.weight.dtype)
        values = values.view(fc.weight.size())
        with torch.no_grad():
            fc.weight.copy_(values)

    return fc


def grouped_optimizer(opt: torch.optim.Optimizer, model: nn.Module,
                      base_lr=0.1, dict_lr: dict = None, no_base=False, **kwargs):
    if dict_lr is None:
        params = model.parameters()
    else:
        np_gen = model.named_parameters

        if no_base:
            params = []
        else:
            base_params = list(map(lambda x: x[1], list(filter(
                lambda kv: kv[0] not in dict_lr.keys(), np_gen()))))
            params = [{'params': base_params}]

        for params_name in dict_lr.keys():
            params.append({'lr': dict_lr[params_name],
                           'params': list(map(lambda x: x[1], list(filter(
                               lambda kv: kv[0] == params_name, np_gen()))))})

    return opt(params, base_lr, **kwargs)


def show_confusion_matrix(data_loader, model=None, ord_models=None,
                          ord_indices=None, criterion=None, args=None):
    cm = calc_confusion_matrix(data_loader, model=model, ord_models=ord_models,
                               ord_indices=ord_indices, criterion=criterion, args=args)
    print(cm)
    acc = calc_acc(cm)
    print("Overall accuracy: {acc:.2%}".format(acc=acc))
    kappa = calc_quadratic_weighted_kappa(cm)
    print('Quadratic weighted kappa = {}'.format(kappa))


def calc_acc(confusion_matrix: torch.Tensor):
    acc = confusion_matrix.trace().item() / confusion_matrix.sum().item()
    return acc


def calc_cohen_kappa(confusion_matrix: torch.Tensor, weighting=None):
    assert isinstance(confusion_matrix, torch.Tensor)
    shape = confusion_matrix.shape
    assert len(shape) == 2 and shape[0] == shape[1]
    n = shape[0]
    confusion_matrix = confusion_matrix.float()

    if weighting is None:
        weights = torch.ones((n, n))
        weights.flatten()[:: n + 1] = 0
    elif weighting == 'linear':
        weights = torch.zeros((n, n)) + torch.arange(n)
        weights = (weights - weights.t()).abs()
    elif weighting == 'quadratic':
        weights = torch.zeros((n, n))
        for i in range(n):
            for j in range(n):
                weights[i][j] = float(((i - j) ** 2) / (n - 1) / (n - 1))
    else:
        raise ValueError(f'Weighting mode {weighting} is not supported.')

    pred = confusion_matrix.sum(dim=0)
    target = confusion_matrix.sum(dim=1)
    expected_matrix = target.ger(pred)

    confusion_matrix /= confusion_matrix.sum()
    expected_matrix /= expected_matrix.sum()
    qwk = 1 - ((confusion_matrix * weights).sum() / (expected_matrix * weights).sum()).item()

    return qwk


def calc_quadratic_weighted_kappa(confusion_matrix: torch.Tensor):
    return calc_cohen_kappa(confusion_matrix, weighting='quadratic')


def calc_confusion_matrix(data_loader, model=None, ord_models=None,
                          ord_indices=None, criterion=None, args=None):
    # switch to evaluate mode
    if model is not None:
        model.eval()

    if ord_models is not None:
        for model in ord_models:
            model.eval()

    # We need a new loader which is not shuffled.
    batch_size = args.batch_size if args is not None else 64
    dataset = data_loader.dataset
    serial_loader = torch.utils.data.DataLoader(
        dataset, batch_size=batch_size, shuffle=False, num_workers=4,
        pin_memory=True, sampler=None)

    with torch.no_grad():
        all_preds = torch.tensor([], device='cuda')  # FIXME
        all_targets = torch.tensor([], device='cuda', dtype=torch.long)
        device = args.gpu if args is not None else None
        for i, (images, target) in enumerate(serial_loader):
            images = images.cuda(device, non_blocking=True)
            target = target.cuda(device, non_blocking=True)

            # compute output
            if ord_models is None:
                output = model(images)
            else:
                sub_logits = []
                for model in ord_models:
                    sub_logits.append(model(images))
                sub_logits = torch.stack(sub_logits).cuda(args.gpu, non_blocking=True)
                output = compute_probabilities(sub_logits, len(ord_indices)).cuda(args.gpu, non_blocking=True)
            # loss = criterion(output, target)

            # _, preds = torch.max(output.data, 1)
            all_preds = torch.cat((all_preds, output), dim=0)
            all_targets = torch.cat((all_targets, target))

        logging.debug("cm: model output computed.")
        stacked = torch.stack(
            (
                all_targets,
                all_preds.argmax(dim=1)
            ),
            dim=1
        ).to('cpu')
        logging.debug("cm: stacked.")

        s = len(dataset.classes)
        confusion_matrix = torch.zeros(s, s, dtype=torch.int64)
        for p in stacked:
            tl, pl = p.tolist()
            pl = (s - 1) if pl >= s else pl
            confusion_matrix[tl, pl] += 1
        logging.debug("cm computed.")

    return confusion_matrix


def apply_model(data_loader, model, image_size=224, n_channel=3):
    model.eval()
    with torch.no_grad():
        outputs = targets = None
        for i, (images, target) in enumerate(data_loader):
            # flatten multiple crops if any
            images = images.view(-1, n_channel, image_size, image_size)
            output = model(images)
            if outputs is None:
                outputs = output
                targets = target
            else:
                outputs = torch.cat((outputs, output))
                targets = torch.cat((targets, target))

    return outputs, targets


def report_grouped_metrics(metrics=('mae', 'ee0', 'ee5', 'ee10'), y=None,
                           data_loader=None, model=None, outputs=None, targets=None,
                           args=None, start: float = 1930, end: float = 1999,
                           group_by=5, show=True, output_logits=True,
                           regression=False):
    """
    Report the metrics of "MAE" (Mean Absolute Error) and "EEn" (absolute
    Estimation Error of at most n years).

    Ground truth of years are provided through the following arguments (in
    descending priority):
        y - A list of years. "outputs" should be specified at the same time.
        targets - A list of class of samples. Years will be then computed
                  using interpolation from the classes.
                  "outputs" must be provided at the same time.
        None - Interpolated from targets, while the latter will be extracted
               from the data_loader.
    Args:
        regression:
        output_logits:
        metrics:
        y:
        data_loader:
        model:
        outputs:
        targets:
        args:
        start:
        end:
        group_by:
        show:

    Returns:

    """
    assert (outputs is not None and (targets is not None or y is not None)
            or data_loader is not None and model is not None
            ), "Either outputs and targets or data_loader and model must " \
               "be provided at the same time."

    if outputs is None:
        assert isinstance(data_loader, torch.utils.data.DataLoader)
        assert isinstance(data_loader.sampler, torch.utils.data.SequentialSampler), \
            "Only simple sequential sampler is allowed."
        outputs, targets = apply_model(data_loader, model, image_size=args.image_size)

    if regression:
        year_estimation = outputs.squeeze()
        y = targets.type(torch.float)
    else:
        if output_logits:
            logits = outputs
            probs = None
        else:
            probs = outputs
            logits = None

        n_classes = outputs.shape[-1]
        year_estimation = calc_year_estimation(n_classes, logits=logits,
                                               probs=probs,
                                               start=start, end=end)
        if y is None:
            y = start + targets * ((end + 1 - start) / n_classes)

    y = y.to(year_estimation.device)
    yhaty_tuple = (year_estimation, y)

    metric_map = {'mae': [calc_mae_grouped, {'start': start, 'end': end, 'group_by': group_by}],
                  'ee0': [calc_ee_n, {'start': start, 'end': end, 'group_by': group_by, 'at_most': 0}],
                  'ee5': [calc_ee_n, {'start': start, 'end': end, 'group_by': group_by, 'at_most': 5}],
                  'ee10': [calc_ee_n, {'start': start, 'end': end, 'group_by': group_by, 'at_most': 10}],
                  }

    metrics = OrderedDict((m,
                           metric_map[m][0](*yhaty_tuple, **metric_map[m][1])
                           ) for m in metrics)

    if show:
        print('Year\t\t{}'.format('\t\t'.join(metrics.keys())))
        for i in range(int((end - start) / group_by) + 1):
            if 'mae' in metrics:
                metric_mae = ['{:.1f}'.format(metrics['mae'][i].cpu())]
            else:
                metric_mae = []

            metrics_een = [
                '{:.1%}'.format(metrics[m][i].cpu()) for m in metrics if m.startswith('ee')
            ]
            metrics_string = '\t\t'.join(metric_mae + metrics_een)

            print('{:d}-{:d}\t{}'.format(
                int(start + i * group_by),
                int(start + (i + 1) * group_by - 1),
                metrics_string
            ))

    return metrics


def report_mae(data_loader=None, model=None, outputs=None, targets=None,
               args=None, start: float = 1930, end: float = 1999,
               group_by=5, show=True):
    result = report_grouped_metrics('mae',
                                    data_loader=data_loader, model=model,
                                    outputs=outputs, targets=targets,
                                    start=start, end=end, group_by=group_by,
                                    show=show, args=args)
    return result['mae']


def calc_year_estimation(n_classes, start: float = 1930,
                         logits=None, probs=None,
                         end: float = 1999, rounding_compensation=0.5):
    assert logits is not None or probs is not None,\
        "Either logits or probs must not be None."
    if probs is None:
        print(f'Shape of logits: {logits.shape}')
        probabilities = F.softmax(logits, dim=1)
    else:
        print(f'Shape of probs: {probs.shape}')
        probabilities = probs

    expected_offsets = torch.sum(
        torch.arange(n_classes, device=probabilities.device).unsqueeze(0).repeat(len(probabilities), 1)
        * probabilities,
        dim=1
    )
    y_estim = start + torch.floor(rounding_compensation + (end - start)
                                  * expected_offsets / (n_classes - 1))

    return y_estim


def calc_abs_year_err(yhat, y, start: float = 1930, end: float = 1999,
                      n_classes=14):
    """

    Args:
        yhat:
        y:
        start:
        end:
        n_classes:

    Returns:

    """
    if not isinstance(yhat, torch.Tensor):
        yhat = torch.tensor(yhat)

    if not isinstance(y, torch.Tensor):
        y = torch.tensor(y)

    assert yhat.shape == y.shape, "Y and X must be in the same shape."
    abs_errors = torch.abs(yhat - y)

    return abs_errors


def calc_mae_grouped(yhat, y, start: float = 1930, end: float = 1999,
                     n_classes=14, group_by=5):
    """

    Args:
        yhat:
        y:
        start:
        end:
        n_classes:
        group_by:

    Returns:

    """
    abs_errors = calc_abs_year_err(yhat, y, start, end, n_classes)

    mean_errors_grouped = []
    # Generally n_groups equals to n_classes, but we are not to assume this.
    n_groups = int((end - start) / group_by) + 1
    for i in range(n_groups):
        mask = (y >= (start + i * group_by)) * (y < (start + (i + 1) * group_by))
        error = (abs_errors * mask).sum() / mask.sum().float()
        mean_errors_grouped.append(error)

    return mean_errors_grouped


def calc_ee_n(yhat, y, start: float = 1930, end: float = 1999,
              n_classes=14, group_by=5, at_most=5):
    """

    Args:
        yhat:
        y:
        start:
        end:
        n_classes:
        group_by:
        at_most:

    Returns:

    """
    abs_errors = calc_abs_year_err(yhat, y, start, end, n_classes)

    ee_n_grouped = []
    # Generally n_groups equals to n_classes, but we are not to assume this.
    n_groups = int((end - start) / group_by) + 1
    for i in range(n_groups):
        mask_year = (y >= (start + i * group_by)) * (y < (start + (i + 1) * group_by))
        mask_errors = abs_errors <= at_most
        mask_errors *= mask_year
        ee_n = mask_errors.sum().float() / mask_year.sum().float()
        ee_n_grouped.append(ee_n)

    return ee_n_grouped


def stratified_weights(dataset, n_classes) -> list:
    count = [0] * n_classes
    for val in dataset:
        count[val[1]] += 1

    weight_per_class = [0.] * n_classes
    N = float(sum(count))
    for val in range(n_classes):
        weight_per_class[val] = N / float(count[val])
    weights = [0] * len(dataset)
    for idx, val in enumerate(dataset):
        weights[idx] = weight_per_class[val[1]]

    return weights


def check_ordinal_argument(dataset: datasets.ImageFolder, args):
    classes = dataset.classes
    classes_in_arg = args.ordinal.split(',')
    if set(classes) == set(classes_in_arg):
        return True

    return False


def load_predefined_dataset(dataset_name, train_transform, val_transform, args=None):
    global writer

    if 'hcl' in dataset_name:
        version = int(dataset_name[len('hcl_v'):])
        root = os.path.join('/Scratch/dh146/hcl-dataset-versioned', f'v{version}')
        if args is not None and args.hcl_options != '':
            import json
            hcl_kwargs = json.loads(args.hcl_options)
        else:
            hcl_kwargs = {}

        if args.regression:
            hcl_kwargs['regression'] = True

        trainset, testset = Hcl.make_dataset(root,
                                             train_transform=train_transform,
                                             val_transform=val_transform,
                                             version=version, **hcl_kwargs)
    elif dataset_name == 'dew':
        trainset, testset = dew.dew_dataset('/Scratch/dh146/DEW-dataset',
                                            train_transform=train_transform,
                                            val_transform=val_transform)
    else:
        trainset = datasets.__dict__[dataset_name](root='/tmp/dh146/datasets', train=True,
                                                   download=True, transform=train_transform)
        testset = datasets.__dict__[dataset_name](root='/tmp/dh146/datasets', train=False,
                                                  download=True, transform=val_transform)

    current_time = datetime.now().strftime('%b%d_%H-%M-%S')
    hostname = os.uname()[1]
    writer = SummaryWriter(os.path.join("runs." + dataset_name,
                                        "{}-{}-{}".format(current_time, hostname, PID)))
    return trainset, testset


def checkpoint_prefix(args):
    cp_prefix = []
    if args.pretrained:
        cp_prefix.append('pt')
    if args.ordinal:
        cp_prefix.append('ord')
    if args.dataset is not None:
        cp_prefix.append(args.dataset)
    if len(cp_prefix) > 0:
        cp_prefix.append('')
    cp_prefix = '-'.join(cp_prefix)

    return cp_prefix


def compare_kappa_calculations(val_loader, model, args=None):
    cm = calc_confusion_matrix(val_loader, model, args=args)
    qwk = calc_quadratic_weighted_kappa(cm)

    outputs, targets = apply_model(val_loader, model, image_size=args.image_size)
    from sklearn.metrics import cohen_kappa_score
    qwk_sk = cohen_kappa_score(targets.clone().cpu().numpy(),
                               outputs.argmax(dim=1).clone().cpu().numpy(),
                               weights='quadratic')
    probs = F.softmax(outputs, dim=-1) if not args.clm else outputs
    qwk_c = cohen_kappa_cont(probs, targets, weighting='quadratic').item()

    lwk_sk = cohen_kappa_score(targets.clone().cpu().numpy(),
                               outputs.argmax(dim=1).clone().cpu().numpy(),
                               weights='linear')
    lwk_c = cohen_kappa_cont(probs, targets, weighting='linear').item()

    print(f'{qwk=:.3f}, {qwk_sk=:.3f}, {qwk_c=:.3f}, {lwk_sk=:.3f}, {lwk_c=:.3f}')

    show_grouped_metrics(val_loader, model, targets=targets, outputs=outputs, args=args)


def show_grouped_metrics(val_loader, model, targets=None, outputs=None, args=None):
    if targets is None or outputs is None:
        outputs, targets = apply_model(val_loader, model, image_size=args.image_size)

    if args is not None and 'hcl' in args.dataset:
        y = torch.tensor(Hcl.years_of('val')).to(outputs.device)
        if args.regression:
            metric_kwargs = {
                'start': Hcl.first_year,
                'end': Hcl.last_year,
                'group_by': Hcl.year_interval,
            }
        else:
            metric_kwargs = {'start': Hcl.first_year - Hcl.year_interval,
                             'end': Hcl.last_year + Hcl.year_interval,
                             'group_by': Hcl.year_interval,
                             'output_logits': not args.clm}
    else:
        y = None
        metric_kwargs = {'output_logits': not args.clm}
    report_grouped_metrics(data_loader=val_loader, model=model,
                           y=y, outputs=outputs, targets=targets,
                           regression=args.regression,
                           **metric_kwargs)


def main_worker(gpu, ngpus_per_node, args: ArgumentParser):
    global best_acc1

    logging.info("Image backend for torchvision: " + get_image_backend())
    logging.warning("Log level: " + logging.getLevelName(logging.root.level))

    # FIXME (DH): This line is copied from the torch example. WTH is this redundancy for?
    args.gpu = gpu

    # Define transformations for data sets
    # FIXME (DH): what is this advprop (advanced propagation?) for?
    if args.advprop:
        normalize = transforms.Lambda(lambda img: img * 2.0 - 1.0)
    else:
        normalize = transforms.Normalize(mean=[0.485, 0.456, 0.406],
                                         std=[0.229, 0.224, 0.225])

    train_transforms = transforms.Compose([
        transforms.RandomResizedCrop(224),
        transforms.RandomHorizontalFlip(),
        transforms.ToTensor(),
        normalize,
    ])

    if 'efficientnet' in args.arch:
        image_size = EfficientNet.get_image_size(args.arch)
        val_transforms = transforms.Compose([
            transforms.Resize(image_size, interpolation=PIL.Image.BICUBIC),
            transforms.CenterCrop(image_size),
            transforms.ToTensor(),
            normalize,
        ])
        args.image_size = image_size
        print('Using image size', image_size)
    else:
        val_transforms = transforms.Compose([
            transforms.Resize(256),
            transforms.CenterCrop(224),
            transforms.ToTensor(),
            normalize,
        ])
        print('Using image size', 224)

    # Load the data sets
    if args.dataset is not None:
        train_dataset, val_dataset = load_predefined_dataset(
            args.dataset, train_transforms, val_transforms, args=args)
    else:
        traindir = os.path.join(args.data, 'train')
        valdir = os.path.join(args.data, 'val')

        train_dataset = datasets.ImageFolder(
            traindir,
            train_transforms)

        val_dataset = datasets.ImageFolder(valdir, val_transforms)

    if not args.regression:
        # Print some information about the dataset.
        print(f'Training set: {len(train_dataset)} samples.')
        print(f'    Class-index mappings:')
        print(f'    {train_dataset.class_to_idx}')

        if args.ordinal is not None:
            if not check_ordinal_argument(train_dataset, args):
                print('Error: Invalid order definition:', args.ordinal)
                print('       Classes in dataset:', train_dataset.classes)

            # Split the dataset
            ord_classes = args.ordinal.split(',')
            class_to_idx = train_dataset.class_to_idx
            ord_indices = [class_to_idx[c] for c in ord_classes]
        else:
            ord_classes = None
            class_to_idx = None
            ord_indices = None

    if args.stratified:
        weights = stratified_weights(train_dataset, len(train_dataset.classes))
        weights = torch.Tensor(weights)
        train_sampler = torch.utils.data.sampler.WeightedRandomSampler(weights, len(weights))
    else:
        train_sampler = None

    train_loader = torch.utils.data.DataLoader(
        train_dataset, batch_size=args.batch_size, shuffle=(train_sampler is None),
        num_workers=args.workers, pin_memory=True, sampler=train_sampler)

    val_loader = torch.utils.data.DataLoader(
        val_dataset,
        batch_size=args.batch_size, shuffle=False,
        num_workers=args.workers, pin_memory=True)

    # create model
    init_weights = (args.resume is None or args.resume == '') and not args.noinit
    if args.ordinal is not None:
        model = None
        ord_models = []
        # We need (k-1) models for k-class
        for i in range(len(ord_indices) - 1):
            print('Setting up for model {}'.format(i))
            if 'efficientnet' in args.arch:
                _model = EfficientNet.from_name(args.arch)
            else:
                model_kwargs = {}
                if 'googlenet' in args.arch:
                    model_kwargs['aux_logits'] = False

                # TODO: Add --no-alt-model logic for ordinal models.
                if args.pretrained:
                    print("=> using pre-trained model '{}'".format(args.arch))
                    _model = models.__dict__[args.arch](pretrained=True, **model_kwargs)
                    # print("=> creating model '{}' without initialization of weights".format(args.arch))
                    # _model = models.__dict__[args.arch](init_weights=False, **model_kwargs)
                    # print('=> loading pretrained weights')
                    # state_dict = load_state_dict_from_url(model_urls['googlenet'],
                    #                                       progress=True)
                    # _model.load_state_dict(state_dict, strict=False)
                else:
                    print("=> creating model '{}'".format(args.arch))
                    _model = models.__dict__[args.arch](init_weights=init_weights, num_classes=2, **model_kwargs)

                if args.last_fc:
                    final_layer = list(_model.children())[-1]
                    for p in _model.parameters():
                        p.requires_grad = False
                    for p in final_layer.parameters():
                        p.requires_grad = True

            ord_models.append(_model)
    else:
        ord_models = None
        if 'efficientnet' in args.arch:  # NEW
            if args.pretrained:
                if args.clm or args.regression:
                    model = EfficientNet.from_pretrained(
                        args.arch, advprop=args.advprop, num_classes=1)
                else:
                    model = EfficientNet.from_pretrained(
                        args.arch, advprop=args.advprop, num_classes=args.n_classes)
                print("=> using pre-trained model '{}'".format(args.arch))
            else:
                if args.clm or args.regression:
                    override_params = {'num_classes': 1}
                else:
                    override_params = {'num_classes': args.n_classes}
                print("=> creating model '{}'".format(args.arch))
                model = EfficientNet.from_name(args.arch, override_params=override_params)

        else:
            model_kwargs = {}
            if 'googlenet' in args.arch:
                model_kwargs['aux_logits'] = False

            if args.pretrained:
                assert not (args.regression and 'googlenet' in args.arch), \
                    'Cannot run pretrained GoogLeNet in regression mode' \
                    ' because the number of outputs is fixed to 1000.'

                print("=> using pre-trained model '{}'".format(args.arch))

                model = models.__dict__[args.arch](pretrained=True, **model_kwargs)

                if args.last_fc:
                    final_layer = list(model.children())[-1]
                    for p in model.parameters():
                        p.requires_grad = False
                    for p in final_layer.parameters():
                        p.requires_grad = True

                if 'googlenet' in args.arch and not args.no_alt_model:
                    assert isinstance(model, models.GoogLeNet)
                    model.fc = create_linear_init(1024, args.n_classes)
            else:
                print("=> creating model '{}'".format(args.arch))
                print(f'{init_weights=}')
                model = models.__dict__[args.arch](init_weights=init_weights,
                                                   num_classes=args.n_classes,
                                                   **model_kwargs)

        if args.clm:
            model = CumulativeLinkLayer.append_to(model, args.n_classes)
            print(f'Model is changed to Ordinal CLM')

        total_params = sum(p.numel() for p in model.parameters() if p.requires_grad)
        print(f'Model has total {total_params:,} trainable parameters.')
        for n, p in model.named_parameters():
            print(f'{n}: {p.numel()}')

    if args.gpu is not None:
        torch.cuda.set_device(args.gpu)
        if args.ordinal is not None:
            for i, m in enumerate(ord_models):
                ord_models[i] = m.cuda(args.gpu)
        else:
            model = model.cuda(args.gpu)
    else:
        # DataParallel will divide and allocate batch_size to all available GPUs
        if args.arch.startswith('alexnet') or args.arch.startswith('vgg'):
            model.features = torch.nn.DataParallel(model.features)
            model.cuda()
        else:
            if args.ordinal is not None:
                for i, m in enumerate(ord_models):
                    ord_models[i] = torch.nn.DataParallel(m).cuda()
            else:
                model = torch.nn.DataParallel(model).cuda()

    # define loss function (criterion) and optimizer
    if args.clm:
        kappa_kwargs = {'input_logits': False}
    else:
        kappa_kwargs = {}

    if args.regression:
        criterion = nn.MSELoss()
    elif args.loss_kappa is not None:
        if args.loss_kappa == 'quadratic':
            criterion = QuadraticWeightedKappaLoss(**kappa_kwargs).cuda(args.gpu)
        elif args.loss_kappa == 'quadratic_log':
            criterion = QuadraticWeightedKappaLoss(activation='log',
                                                   **kappa_kwargs).cuda(args.gpu)
        elif args.loss_kappa == 'linear':
            criterion = LinearWeightedKappaLoss(**kappa_kwargs).cuda(args.gpu)
        else:
            raise ValueError(f'{args.loss_kappa=} is not supported.')
    else:
        criterion = nn.CrossEntropyLoss().cuda(args.gpu)

    if args.transfer is not None:
        if args.transfer.startswith('http'):
            url = args.transfer
            local = None
        else:
            local = args.transfer
            url = None

        if 'efficientnet' in args.arch:
            load_efficientnet_weights_from(model, url=url, local_file=local, load_fc=False)
        else:
            raise NotImplementedError('Transfer architectures other than EfficienNet'
                                      'has not been implemented yet.')

    if args.ordinal is not None:
        optimizer = None
        ord_opts = list()
        for m in ord_models:
            ord_opts.append(torch.optim.SGD(m.parameters(), args.lr,
                                            momentum=args.momentum,
                                            weight_decay=args.weight_decay))
    else:
        ord_opts = None
        if args.no_grouped_opt:
            optimizer = torch.optim.SGD(model.parameters(), args.lr,
                                        momentum=args.momentum,
                                        weight_decay=args.weight_decay)
        else:
            if args.pretrained:
                if args.last_fc:
                    optimizer = grouped_optimizer(torch.optim.SGD, model, args.lr,
                                                  {'module.fc.weight': args.lr,
                                                   'module.fc.bias': args.lr},
                                                  no_base=True,
                                                  momentum=args.momentum,
                                                  weight_decay=args.weight_decay)
                else:
                    optimizer = grouped_optimizer(torch.optim.SGD, model, args.lr,
                                                  {'module.fc.weight': args.lr * 10,
                                                   'module.fc.bias': args.lr * 10},
                                                  momentum=args.momentum,
                                                  weight_decay=args.weight_decay)
            else:
                optimizer = torch.optim.SGD(model.parameters(), args.lr,
                                            momentum=args.momentum,
                                            weight_decay=args.weight_decay)

    # optionally resume from a checkpoint
    # TODO: add save/load support for ordinal model
    if args.resume:
        if os.path.isfile(args.resume):
            print("=> loading checkpoint '{}'".format(args.resume))
            checkpoint = torch.load(args.resume)
            if 'epoch' in checkpoint.keys():
                args.start_epoch = checkpoint['epoch']
                print('epoch recovered')
            if 'best_acc1' in checkpoint.keys():
                best_acc1 = checkpoint['best_acc1']
                print('best_acc1 recovered')
                if args.gpu is not None:
                    # best_acc1 may be from a checkpoint from a different GPU
                    best_acc1 = best_acc1.to(args.gpu)
            if 'state_dict' in checkpoint.keys():
                model.load_state_dict(checkpoint['state_dict'])
                print('state_dict recovered.')
            else:
                logging.warning("Resuming checkpoint without model dict!")

            if 'optimizer' in checkpoint.keys() and not args.report_mode:
                optimizer.load_state_dict(checkpoint['optimizer'])
                print('optimizer recovered.')
            print("=> loaded checkpoint '{}' (epoch {})"
                  .format(args.resume, checkpoint['epoch']))
        else:
            print("=> File '{}' cannot be found.".format(args.resume))

    if args.regression:
        class RegressionModel(type(model)):
            def __init__(self, **kwargs):
                super(RegressionModel, self).__init__(**kwargs)
                self.bias = torch.tensor(1950)
                self.slope = torch.tensor(100)

            def forward(self, inputs, **kwargs):
                x = super(RegressionModel, self).forward(inputs, **kwargs)
                return x * self.slope + self.bias

        class SimpleLinear(nn.Module):
            bias = torch.tensor(1950)
            slope = torch.tensor(100)

            def forward(self, inputs):
                return inputs * self.slope + self.bias

        model = nn.Sequential(OrderedDict({'base': model, 'linear': SimpleLinear()}))

    cudnn.benchmark = True

    if args.evaluate:
        res = validate(val_loader, model, criterion, args)
        with open('res.txt', 'w') as f:
            print(res, file=f)
        return

    for epoch in range(args.start_epoch, args.epochs):
        logging.info("Epoch {} started.".format(epoch))
        if args.distributed:
            train_sampler.set_epoch(epoch)

        is_ordinal = args.ordinal is not None

        opts = ord_opts if is_ordinal else [optimizer]
        adjust_learning_rate(opts, epoch, args, is_ordinal)

        # train for one epoch
        logging.debug("Training stage...")
        if is_ordinal:
            train_ordinal(train_loader, ord_models, ord_indices,
                          criterion, ord_opts, epoch, args)

            acc1 = validate_ordinal(val_loader, ord_models, ord_indices,
                                    criterion, epoch, args)
        else:
            train(train_loader, model, criterion, optimizer, epoch, args)

            # evaluate on validation set
            acc1 = validate(val_loader, model, criterion, args)
            writer.add_scalar('Validation/Acc1', acc1, epoch)
            writer.flush()

        logging.debug("Training stage of epoch {} completed.".format(epoch))

        # remember best acc@1 and save checkpoint
        is_best = acc1 > best_acc1
        best_acc1 = max(acc1, best_acc1)

        if not is_ordinal and (
                not args.multiprocessing_distributed or (
                args.multiprocessing_distributed and args.rank % ngpus_per_node == 0)):
            cp_prefix = checkpoint_prefix(args)

            save_checkpoint({
                'epoch': epoch + 1,
                'arch': args.arch,
                'state_dict': model.state_dict(),
                'best_acc1': best_acc1,
                'optimizer': optimizer.state_dict(),
            }, is_best, prefix=cp_prefix)

        if args.regression:
            show_grouped_metrics(val_loader, model, args=args)
        else:
            if not is_ordinal:
                # print('Confusion matrix on training set:')
                # show_confusion_matrix(train_loader, model=model, criterion=criterion, args=args)
                print('Confusion matrix on validation set:')
                show_confusion_matrix(val_loader, model=model, criterion=criterion, args=args)
                if args.dataset is None or args.dataset != 'dew':
                    compare_kappa_calculations(val_loader, model, args=args)
            else:
                print('Confusion matrix on training set:')
                show_confusion_matrix(train_loader, ord_models=ord_models, ord_indices=ord_indices,
                                      criterion=criterion, args=args)
                print('Confusion matrix on validation set:')
                show_confusion_matrix(val_loader, ord_models=ord_models, ord_indices=ord_indices,
                                      criterion=criterion, args=args)

    print('Model finished at', time.time())

    return model, train_loader, val_loader, criterion, args


def convert_sub_target(target, ord_indices, sub_index):
    ord_indices_inverse = [-1] * len(ord_indices)
    for i in range(len(ord_indices)):
        ord_indices_inverse[ord_indices[i]] = i

    target_inverse = target.clone().cpu()
    target_inverse.map_(target_inverse, lambda x, _: ord_indices_inverse[x])

    derived_target = (target_inverse > sub_index) * 1
    return derived_target


def monotonic_smoothing(sub_logits: torch.Tensor, rows=(1,), descending=(True,)) -> torch.Tensor:
    """
    Using the heuristic smoothing method proposed by (Schapire et. al., 2002) to refine the output
    so that they are monotonically decreasing (if descending=True) or increasing (if descending=False).

    Args:
        sub_logits: logits predictions in Tensor with the size of (c, m, b), where
                c - the number of the derived classifiers (n_classes-1)
                m - the number of instances
                b - commonly 2 for binary sub-classifiers.
        rows: A tuple specifying which row in the highest dimension to be smoothed.
        descending:

    Returns: Smoothed logits as Tensor.

    """
    # dim0: class, dim1: instance, dim2: binary
    input_size = sub_logits.size()
    # output_size = list(input_size[:-1]) + [len(rows)]
    output_size = torch.Size(list(input_size[:-1]) + [0])

    last_dim = list(input_size)[-1]
    smoothed = torch.empty(output_size, dtype=sub_logits.dtype)
    for i, row in enumerate(rows):
        # dim0: class, dim1: instance
        f = sub_logits[:, :, row]

        f_prime = torch.empty(f.size(), dtype=f.dtype)
        for j in range(f.size()[0]):
            min_ub, _ = f[j:].max(dim=0)
            max_lb, _ = f[:j + 1].min(dim=0)
            f_prime[j] = (min_ub + max_lb) / 2.

        f_prime = f_prime.unsqueeze(last_dim)
        smoothed = torch.cat((smoothed, f_prime), dim=last_dim)

    return smoothed


def compute_probabilities(sub_logits: torch.Tensor, n_classes: int, smoothing=True) -> torch.Tensor:
    sub_probs = []

    print(f'{sub_logits.shape=}\nbefore smoothing: {sub_logits[:, :2, :]=}')
    if smoothing:
        # after smoothing: dim0: class, dim1: instance
        sub_logits = monotonic_smoothing(sub_logits).squeeze(dim=-1)
        print(f'{sub_logits.shape=}\nafter smoothing: {sub_logits[:, :2]=}')
        for logit in sub_logits:
            sub_probs.append(torch.sigmoid(logit))
    else:
        for logit in sub_logits:
            sub_probs.append(F.softmax(logit, dim=1))

    probs = []
    probs.append(1 - sub_probs[0])
    for i in range(1, n_classes - 1):
        probs.append(sub_probs[i - 1] - sub_probs[i])
    probs.append(sub_probs[-1])
    # output: dim0: instance, dim1: class
    output = torch.stack(probs, dim=1)

    print(f'{torch.stack(sub_probs, dim=1)[:2, :]=}')
    print(f'{torch.stack(probs, dim=1)[:2, :]=}')
    print('probability output ={}'.format(output[:2, :]))

    return output


def idx_at_separation(ord_indices, sub_index):
    ord_indices_inverse = [-1] * len(ord_indices)
    for i in range(len(ord_indices)):
        ord_indices_inverse[ord_indices[i]] = i

    return ord_indices_inverse[sub_index]


def train_ordinal(train_loader, ord_models, ord_indices, criterion,
                  optimizer, epoch, args):
    for model in ord_models:
        model.train()

    # ord_indices_inverse stores a reverse mapping from the original class
    # index to the ordered class index.
    ord_indices_inverse = [-1] * len(ord_indices)
    for i in range(len(ord_indices)):
        ord_indices_inverse[ord_indices[i]] = i
    for i, (images, target) in enumerate(train_loader):
        # Images are the same
        if args.gpu is not None:
            images = images.cuda(args.gpu, non_blocking=True)

        target = target.cuda(args.gpu, non_blocking=True)
        # Currently only optimization in derived models is performed,
        # which means they are individually optimized.
        # TODO: examine the derivability of global loss function.
        sub_logits = []
        for j, model in enumerate(ord_models):
            class_name = train_loader.dataset.classes[
                idx_at_separation(ord_indices, j)]
            derived_target = convert_sub_target(target, ord_indices, j)
            derived_target = derived_target.cuda(args.gpu, non_blocking=True)

            output = model(images)
            if args.pretrained:
                sub_logits.append(output[:, :2])
            else:
                sub_logits.append(output)
            if epoch < 2:
                print(f'{output[:, :2].shape=}\n{output[:, :2]=}')
                print(f'{output.shape=}\n{output=}')

            loss = criterion(output, derived_target)

            # Compute gradient and SGD step
            optimizer[j].zero_grad()
            loss.backward()
            optimizer[j].step()

            acc1 = accuracy(output, derived_target, topk=(1,))

            writer.add_scalar('TrainSubLoss/' + class_name, loss, epoch)
            writer.add_scalar('TrainSubAcc1/' + class_name, acc1[0].item(), epoch)

            print('Epoch {} [{}/{}] sub-class {} (boundary {}): loss={}, acc={}'.format(
                epoch, i, len(train_loader), j, class_name, loss, acc1[0].item()))

        output = compute_probabilities(torch.stack(sub_logits), len(ord_indices)).cuda(args.gpu, non_blocking=True)
        acc1, acc2 = accuracy(output, target, topk=(1, 2))
        writer.flush()
        print('Epoch {} [{}/{}]: acc1={}, acc2={}'.format(
            epoch, i, len(train_loader), acc1.item(), acc2.item()))


def plot_grad_flow(named_parameters, output):
    ave_grads = []
    layers = []
    for n, p in named_parameters:
        if p.requires_grad and ("bias" not in n):
            label = n[7:] if n[:7] == 'module.' else n
            layers.append(label)
            ave_grads.append(p.grad.abs().mean())
    import matplotlib.pyplot as plt
    from matplotlib import rcParams
    rcParams.update({'figure.autolayout': True})

    plt.gcf().set_size_inches(18.5, 10.5)
    plt.plot(ave_grads, alpha=0.3, color="b")
    plt.hlines(0, 0, len(ave_grads) + 1, linewidth=1, color="k")
    plt.xticks(range(0, len(ave_grads), 1), layers, rotation="vertical")
    plt.xlim(xmin=0, xmax=len(ave_grads))
    plt.xlabel("Layers")
    plt.ylabel("average gradient")
    plt.yscale('log')
    plt.title("Gradient flow")
    plt.grid(True)
    plt.savefig(output, dpi=300)
    plt.close(plt.gcf())


def train(train_loader, model, criterion, optimizer, epoch, args):
    batch_time = AverageMeter('Time', ':6.3f')
    data_time = AverageMeter('Data', ':6.3f')
    losses = AverageMeter('Loss', ':.4e')
    top1 = AverageMeter('Acc@1', ':6.2f')
    top5 = AverageMeter('Acc@{}'.format(k_top), ':6.2f')
    progress = ProgressMeter(len(train_loader), batch_time, data_time, losses, top1,
                             top5, prefix="Epoch: [{}]".format(epoch))

    # switch to train mode
    model.train()

    end = time.time()
    for i, (images, target) in enumerate(train_loader):
        if args.show_batch_info:
            print('{} epoch {} training [{}/{}] started.'.format(
                datetime.now().strftime("%H:%M:%S"), epoch, i, len(train_loader)
            ))
        # measure data loading time
        data_time.update(time.time() - end)

        if args.show_batch_info:
            t_count = dict()
            class_to_idx = train_loader.dataset.class_to_idx
            for c in class_to_idx.values():
                cname = [x for x in class_to_idx if class_to_idx[x] == c][0]
                t_count[cname] = (target == c).sum().item()

            print('Batch distribution:', t_count)

        if args.gpu is not None:
            images = images.cuda(args.gpu, non_blocking=True)
        target = target.cuda(args.gpu, non_blocking=True)

        # compute output
        output = model(images)
        if args.regression:
            output = output.squeeze()
            target = target.type(torch.float)

        if args.show_batch_info:
            print('{} epoch {} batch [{}/{}] model output generated.'.format(
                datetime.now().strftime("%H:%M:%S"), epoch, i, len(train_loader)
            ))
        # if 'googlenet' in args.arch:
        #     output = output.logits
        loss = criterion(output, target)

        losses.update(loss.item(), images.size(0))
        if not args.regression:
            # measure accuracy and record loss
            acc1, acc5 = accuracy(output, target, topk=(1, k_top))
            top1.update(acc1[0], images.size(0))
            top5.update(acc5[0], images.size(0))

        if args.show_batch_info:
            assert isinstance(writer, SummaryWriter)
            writer.add_scalar('TrainBatch/Acc1', acc1[0], epoch * len(train_loader) + i)
            writer.add_scalar('TrainBatch/Loss', loss.item(), epoch * len(train_loader) + i)
            writer.flush()

        # compute gradient and do SGD step
        optimizer.zero_grad()
        loss.backward()

        if args.show_batch_info:
            print('{} epoch {} batch [{}/{}] back-propagated.'.format(
                datetime.now().strftime("%H:%M:%S"), epoch, i, len(train_loader)
            ))

        if args.plot_grad:
            plot_grad_flow(model.named_parameters(), args.plot_grad + f'-epoch_{epoch:03d}')
        for n, p in model.named_parameters():
            if n.startswith('module.clm'):
                print(f'Gradients of {n}: {p.grad=}')

        optimizer.step()

        if args.show_batch_info:
            print('{} epoch {} batch [{}/{}] optimization completed.'.format(
                datetime.now().strftime("%H:%M:%S"), epoch, i, len(train_loader)
            ))

        # measure elapsed time
        batch_time.update(time.time() - end)
        end = time.time()

        if i % args.print_freq == 0:
            progress.print(i)

        if args.show_batch_info:
            print('{} epoch {} batch [{}/{}] batch training completed.'.format(
                datetime.now().strftime("%H:%M:%S"), epoch, i, len(train_loader)
            ))

    if not args.regression:
        writer.add_scalar('Train/Acc1', top1.avg, epoch)
    writer.add_scalar('Train/Loss', losses.avg, epoch)
    writer.flush()


def validate_ordinal(val_loader, ord_models, ord_indices, criterion, epoch, args):
    # Switch to evaluate mode
    for model in ord_models:
        model.eval()

    with torch.no_grad():
        for i, (images, target) in enumerate(val_loader):
            if args.gpu is not None:
                images = images.cuda(args.gpu, non_blocking=True)
            target = target.cuda(args.gpu, non_blocking=True)
            sub_logits = []
            for j, model in enumerate(ord_models):
                class_name = val_loader.dataset.classes[ord_indices.index(j)]
                derived_target = convert_sub_target(target, ord_indices, j)
                derived_target = derived_target.cuda(args.gpu, non_blocking=True)

                output = model(images)
                loss = criterion(output, derived_target)

                if args.pretrained:
                    sub_logits.append(output[:, :2])
                else:
                    sub_logits.append(output)

                acc1 = accuracy(output, derived_target, topk=(1,))

                writer.add_scalar('ValSubLoss/' + class_name, loss, epoch)
                writer.add_scalar('ValSubAcc1/' + class_name, acc1[0].item(), epoch)
                print('Epoch {} [{}/{}] sub-class {} (boundary {}): loss={}, acc={}'.format(
                    epoch, i, len(val_loader), j, class_name, loss, acc1[0].item()))

            writer.flush()
            output = compute_probabilities(torch.stack(sub_logits), len(ord_indices))
            acc1, acc2 = accuracy(output, target, topk=(1, 2))
            print('Test: acc1={}, acc2={}'.format(acc1[0].item(), acc2[0].item()))

        assert isinstance(acc1, torch.Tensor)
        return acc1.mean().item()


def validate(val_loader, model, criterion, args):
    batch_time = AverageMeter('Time', ':6.3f')
    losses = AverageMeter('Loss', ':.4e')
    top1 = AverageMeter('Acc@1', ':6.2f')
    top5 = AverageMeter('Acc@{}'.format(k_top), ':6.2f')
    progress = ProgressMeter(len(val_loader), batch_time, losses, top1, top5,
                             prefix='Test: ')

    # switch to evaluate mode
    model.eval()

    with torch.no_grad():
        end = time.time()
        for i, (images, target) in enumerate(val_loader):
            if args.gpu is not None:
                images = images.cuda(args.gpu, non_blocking=True)
            target = target.cuda(args.gpu, non_blocking=True)

            # compute output
            output = model(images)
            if args.regression:
                output = output.squeeze()
                target = target.type(torch.float)
            loss = criterion(output, target)

            losses.update(loss.item(), images.size(0))
            if not args.regression:
                # measure accuracy and record loss
                acc1, acc5 = accuracy(output, target, topk=(1, k_top))
                top1.update(acc1[0], images.size(0))
                top5.update(acc5[0], images.size(0))

            # measure elapsed time
            batch_time.update(time.time() - end)
            end = time.time()

            if i % args.print_freq == 0:
                progress.print(i)

        if not args.regression:
            # TODO: this should also be done with the ProgressMeter
            print(' * Acc@1 {top1.avg:.3f} Acc@{k_top} {top5.avg:.3f}'
                  .format(top1=top1, top5=top5, k_top=k_top))

    return top1.avg


def save_checkpoint(state, is_best, filename=PID + '.checkpoint.pth.tar', prefix=''):
    filename = prefix + filename
    torch.save(state, filename)
    if is_best:
        shutil.copyfile(filename, prefix + PID + '.model_best.pth.tar')


class AverageMeter(object):
    """Computes and stores the average and current value"""

    def __init__(self, name, fmt=':f'):
        self.name = name
        self.fmt = fmt
        self.reset()

    def reset(self):
        self.val = 0
        self.avg = 0
        self.sum = 0
        self.count = 0

    def update(self, val, n=1):
        self.val = val
        self.sum += val * n
        self.count += n
        self.avg = self.sum / self.count

    def __str__(self):
        fmtstr = '{name} {val' + self.fmt + '} ({avg' + self.fmt + '})'
        return fmtstr.format(**self.__dict__)


class ProgressMeter(object):
    def __init__(self, num_batches, *meters, prefix=""):
        self.batch_fmtstr = self._get_batch_fmtstr(num_batches)
        self.meters = meters
        self.prefix = prefix

    def print(self, batch):
        entries = [self.prefix + self.batch_fmtstr.format(batch)]
        entries += [str(meter) for meter in self.meters]
        print('\t'.join(entries))

    def _get_batch_fmtstr(self, num_batches):
        num_digits = len(str(num_batches // 1))
        fmt = '{:' + str(num_digits) + 'd}'
        return '[' + fmt + '/' + fmt.format(num_batches) + ']'


def adjust_learning_rate(opts, epoch, args, is_ordinal=False):
    """Sets the learning rate to the initial LR decayed by 10 every 30 epochs"""
    # lr = args.lr * (0.5 ** (epoch // 30))
    for opt in opts:
        for param_group in opt.param_groups:
            lr = param_group['lr']
            print(f'Current {lr=}')

    if epoch % args.lr_interval != 0 or epoch == 0:
        return

    for opt in opts:
        for param_group in opt.param_groups:
            lr = param_group['lr']
            lr *= 0.5
            print(f'New {lr=}')
            param_group['lr'] = lr


def accuracy(output, target, topk=(1,)):
    """Computes the accuracy over the k top predictions for the specified values of k"""
    with torch.no_grad():
        maxk = max(topk)
        batch_size = target.size(0)

        _, pred = output.topk(maxk, 1, True, True)
        pred = pred.t()
        target = target.to(pred.device)
        correct = pred.eq(target.view(1, -1).expand_as(pred))

        res = []
        for k in topk:
            correct_k = correct[:k].view(-1).float().sum(0, keepdim=True)
            res.append(correct_k.mul_(100.0 / batch_size))
        return res


if __name__ == '__main__':
    result = main()
