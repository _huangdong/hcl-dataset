# -*- coding: utf-8 -*-
from typing import Any
from collections import OrderedDict

import torch
import torch.nn as nn


def inverse_cloglog(x: torch.Tensor) -> torch.Tensor:
    return torch.tensor(1.) - (torch.tensor(1.) / x.exp().exp())


class CumulativeLinkLayer(nn.Module):
    """
    References: [1] Vargas, V. M., Gutiérrez, P. A., & Hervás-Martínez,
        C. (2020). Cumulative link models for deep ordinal classification.
        Neurocomputing. https://doi.org/10.1016/j.neucom.2020.03.034

    """
    def __init__(self, n_classes: int, link_func: str = 'sigmoid',
                 amp_a: float = 1, amp_b: float = 1
                 ) -> None:
        super().__init__()
        self.n_classes = n_classes

        self.link_func = link_func
        if link_func == 'sigmoid':
            self.f = torch.sigmoid
        elif link_func == 'probit':
            raise NotImplementedError(
                'Link function of "probit" has not been implemented yet.')
        elif link_func == 'clog-log':
            self.f = inverse_cloglog
        else:
            raise ValueError(f'{link_func} is not a correct link function.')

        self.amp_a = amp_a
        self.amp_b = amp_b

        self.coeff_a = nn.Parameter(self.init_coeff_a(), requires_grad=True)
        self.register_parameter('a', self.coeff_a)
        self.coeff_b = nn.Parameter(self.init_coeff_b(), requires_grad=True)
        self.register_parameter('b', self.coeff_b)

    def forward(self, input: torch.Tensor, **kwargs: Any):
        b = self.boundaries().to(input.device)
        cum_dist_func = self.f(b - input)

        shape = list(input.shape[:-1]) + [1]

        cdf_right = torch.cat((cum_dist_func, torch.ones(
            shape, device=input.device)), dim=-1)
        cdf_left = torch.cat((torch.zeros(
            shape, device=input.device), cum_dist_func), dim=-1)
        probs = cdf_right - cdf_left

        return probs

    def boundaries(self):
        return self.coeff_b * self.amp_b + (
                (self.coeff_a * self.amp_a) ** 2).cumsum(dim=-1)

    def init_coeff_a(self) -> torch.Tensor:
        # return torch.arange(0, self.n_classes - 1, dtype=torch.float) / self.amp_a
        return torch.ones(self.n_classes - 1, dtype=torch.float) / self.amp_a

    def init_coeff_b(self) -> torch.Tensor:
        return torch.ones(1) * (-self.n_classes / 2) / self.amp_b

    @classmethod
    def append_to(cls, model: nn.Module, n_features: int):
        clm = CumulativeLinkLayer(n_features)
        return nn.Sequential(OrderedDict({'base': model, 'clm': clm}))

