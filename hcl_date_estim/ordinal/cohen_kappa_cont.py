from typing import Any

import torch
from torch.nn import Module
import logging


# from torch.nn.modules.module import T_co


def _one_minus_kappa_cont(prob: torch.Tensor, target: torch.Tensor,
                          n_classes=-1, weighting=None) -> torch.Tensor:
    # All computation is on prob's device.
    device = prob.device
    target.to(device)

    if n_classes == -1:
        n_classes = prob.shape[-1]

    n_samples = torch.tensor(target.shape).prod()

    if weighting is None:
        weighted = None
    elif weighting == 'linear':
        weighted = torch.abs
    elif weighting == 'quadratic':
        def weighted(x): return x ** 2
    else:
        raise ValueError(f'{weighting=} is not supported.')

    # TODO: Add support for 3-d and higher dimensional outputs.
    #       - A 2-d output is the normal form of (instances, classes)
    #       - A 3-d output, for example, may contain multiple crops per
    #         instance, namely (instances, crops, classes).
    assert prob.ndim == 2, \
        f'''
            Dimension of {prob.ndim} has not been supported yet.
            Viewing this output of size {tuple(prob.shape)} by
            .view({n_samples.item()}, {n_classes}) may work around the problem.
        '''

    # w_tq: weights centred to the correct classes, 2-dimensional tensor.
    # Dimensions of w_tq:
    #   0 - instances
    #   1 - classes
    if weighted is not None:
        w_tq = torch.cat([weighted(torch.arange(-t.item(), -t + n_classes))
                         .view(1, n_classes) for t in target])
    else:
        w_tq = torch.ones((n_samples.item(), n_classes))
        for i, t in enumerate(target):
            w_tq[i, t] = 0

    w_tq = w_tq.to(device)
    numerator = torch.sum(w_tq * prob)

    target_histogram = torch.tensor([torch.sum(target == c) for c in range(n_classes)],
                                    dtype=torch.float) / n_samples
    target_histogram = target_histogram.to(device)
    # w_ij: weights, 3-dimensional tensor sized Qx1xQ.
    # Dimensions of w_ij:
    #   0 - target classes
    #   1 - instances (defined with 1 element, to be broadcast to N)
    #   2 - prediction classes
    if weighted is not None:
        w_ij = torch.cat([weighted(torch.arange(-i, -i + n_classes))
                         .view(1, 1, n_classes)
                          for i in range(n_classes)], dim=0)
    else:
        w_ij = torch.ones(n_classes ** 2)
        w_ij[:: n_classes + 1] = 0.
        w_ij = w_ij.view((n_classes, 1, n_classes))

    w_ij = w_ij.to(device)

    # The NxQ prob is unsqueezed to 1xNxQ, multiplying w_ij in Qx1xQ,
    # producing a QxNxQ result. Summing the QxNxQ along all dimensions
    # except 0, collapsing all the inner dimensions (d > 0), produces a 1-D
    # tensor of shape (Q,), each of whose elements represents the total
    # number of weighted predictions for a target class, equivalent to
    # \sum_{j=1}^Q (W_{i,j} E_{i,j}) in the non-continuous form.
    pred_per_target = torch.sum(w_ij * prob.unsqueeze(dim=0),
                                dim=list(range(1, w_ij.ndim)))
    denominator = torch.sum(pred_per_target * target_histogram)

    return numerator / denominator


def cohen_kappa_cont(prob: torch.Tensor, target: torch.Tensor, n_classes=-1,
                     weighting=None) -> torch.Tensor:
    """
    Args:
        prob: Output probabilities.
        target: Targets of samples.
        n_classes: Number of classes. If not specified, compute from the length
                    of prob.
        weighting: None for unweighted, 'linear' or 'quadratic' for weighted.

    Returns:
        A scalar cohen kappa metric with specified weighting.

    """
    return torch.tensor(1.) - _one_minus_kappa_cont(prob, target, n_classes, weighting)


class KappaLoss(Module):
    def __init__(self, input_logits=True, weighting=None, activation='identity',
                 **kwargs) -> None:
        """

        Args:
            input_logits:
            weighting:
            activation: 'identity' or 'log'
            **kwargs:
        """
        super().__init__()

        reduction = kwargs.get('reduction', None)
        if reduction is not None:
            logging.warning(f'"reduction" is specified but takes no effects.')

        self.input_logits = input_logits
        self.weighting = weighting
        self.activation = activation

    def forward(self, input: torch.Tensor, target, **kwargs: Any):
        if self.input_logits:
            prob = torch.softmax(input, dim=-1)
        else:
            prob = input

        one_minus_kappa = _one_minus_kappa_cont(prob, target, weighting=self.weighting)
        if self.activation == 'log':
            one_minus_kappa = one_minus_kappa.log()
        elif self.activation != 'identity':
            raise ValueError(f'{self.activation=} is invalid.')

        return one_minus_kappa


class QuadraticWeightedKappaLoss(KappaLoss):

    def __init__(self, input_logits=True, **kwargs) -> None:
        super().__init__(input_logits, weighting='quadratic', **kwargs)


class LinearWeightedKappaLoss(KappaLoss):
    def __init__(self, input_logits=True, **kwargs) -> None:
        super().__init__(input_logits, weighting='linear', **kwargs)
